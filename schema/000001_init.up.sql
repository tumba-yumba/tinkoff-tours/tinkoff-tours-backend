CREATE TYPE ROLE AS ENUM (
    'guide',
    'admin'
    );

CREATE TABLE IF NOT EXISTS users
(
    id            uuid PRIMARY KEY,
    ROLE          ROLE                    NOT NULL,
    first_name    varchar(100),
    last_name     varchar(100),
    email         varchar(100) UNIQUE     NOT NULL,
    password_hash varchar                 NOT NULL,
    created_at    timestamp DEFAULT now() NOT NULL,
    bio           varchar   DEFAULT ''    NOT NULL
);

CREATE TABLE IF NOT EXISTS tours
(
    id                uuid PRIMARY KEY,
    guide_id          uuid REFERENCES users (id) ON DELETE CASCADE,
    title             varchar(100) NOT NULL,
    description       varchar      NOT NULL,
    short_description varchar(255) NOT NULL,
    country           varchar(100) NOT NULL,
    city              varchar(100) NOT NULL,
    price             integer      NOT NULL,
    max_participants  integer      NOT NULL,
    start_time        timestamp    NOT NULL,
    end_time          timestamp    NOT NULL
);

CREATE TABLE IF NOT EXISTS tags
(
    id    serial PRIMARY KEY,
    title varchar(100) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS tours_tags
(
    tour_id uuid REFERENCES tours (id) ON DELETE CASCADE,
    tag_id  serial REFERENCES tags (id) ON DELETE CASCADE,
    CONSTRAINT tour_tag_pk PRIMARY KEY (tour_id, tag_id)
);

CREATE TABLE IF NOT EXISTS photos
(
    id       uuid PRIMARY KEY,
    tour_id  uuid REFERENCES tours (id) ON DELETE CASCADE,
    filename varchar NOT NULL
);

CREATE TABLE IF NOT EXISTS issued_tokens
(
    jti        varchar PRIMARY KEY NOT NULL,
    user_id    uuid REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE,
    is_revoked boolean DEFAULT FALSE
);

