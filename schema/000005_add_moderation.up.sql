CREATE TYPE moderate_status AS ENUM (
    'pending',
    'approved',
    'rejected'
    );

ALTER TABLE IF EXISTS tours
    ADD COLUMN moderate_status moderate_status DEFAULT 'pending' NOT NULL;

ALTER TABLE IF EXISTS users
    ADD COLUMN moderate_status moderate_status DEFAULT 'rejected' NOT NULL;

