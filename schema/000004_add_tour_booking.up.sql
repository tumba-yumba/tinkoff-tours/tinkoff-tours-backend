CREATE TABLE IF NOT EXISTS tour_booking
(
    id                 uuid PRIMARY KEY,
    tour_id            uuid REFERENCES tours (id) ON DELETE CASCADE,
    email              varchar(100) NOT NULL,
    name               varchar(100) NOT NULL,
    participants_count int          NOT NULL
);

