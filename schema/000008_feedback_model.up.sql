CREATE TABLE IF NOT EXISTS feedback
(
    id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    tour_id uuid REFERENCES tours (id) ON DELETE CASCADE,
    booking_id uuid REFERENCES tour_booking (id) ON DELETE CASCADE,

    comment varchar NOT NULL,

    guide_rating integer NOT NULL,
    tour_rating integer NOT NULL,

    -- guide estimations
    boring_estimation boolean NOT NULL,
    know_nothing_estimation boolean NOT NULL,
    not_heard_estimation boolean NOT NULL,
    interesting_estimation boolean NOT NULL,
    know_everything_estimation boolean NOT NULL,
    entertainment_estimation boolean NOT NULL,
    
    -- tour estimations
    not_impressed_estimation boolean NOT NULL,
    bad_weather_estimation boolean NOT NULL,
    uncomfortable_estimation boolean NOT NULL,
    picturesque_estimation boolean NOT NULL,
    comfortable_estimation boolean NOT NULL,
    adrenaline_estimation boolean NOT NULL,

    created_at timestamp NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS feedback_photos
(
    id uuid PRIMARY KEY,
    feedback_id uuid REFERENCES feedback (id) ON DELETE CASCADE,
    filename varchar NOT NULL
);