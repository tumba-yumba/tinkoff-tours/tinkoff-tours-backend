CREATE TABLE IF NOT EXISTS tour_times
(
    id         uuid PRIMARY KEY,
    tour_id    uuid REFERENCES tours (id) ON DELETE CASCADE,
    start_time timestamp NOT NULL,
    end_time   timestamp NOT NULL
);

ALTER TABLE tours
    DROP COLUMN start_time,
    DROP COLUMN end_time;

ALTER TABLE tour_booking
    ADD COLUMN IF NOT EXISTS tour_time_id uuid REFERENCES tour_times (id) ON DELETE CASCADE,
    ADD COLUMN IF NOT EXISTS is_email_send boolean DEFAULT false;
