ALTER TABLE IF EXISTS tours
    DROP COLUMN moderate_status;

ALTER TABLE IF EXISTS users
    DROP COLUMN moderate_status;

DROP TYPE IF EXISTS moderate_status;

