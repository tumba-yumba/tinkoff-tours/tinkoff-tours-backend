ALTER TABLE IF EXISTS tour_booking
    DROP COLUMN IF EXISTS tour_time_id,
    DROP COLUMN IF EXISTS is_email_send;

ALTER TABLE tours
    ADD COLUMN start_time timestamp NOT NULL DEFAULT now(),
    ADD COLUMN end_time   timestamp NOT NULL DEFAULT now();

DROP TABLE IF EXISTS tour_times;

