package db

import (
	"fmt"
	"tinkoff-tours/configs"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func New(cfg *configs.Config) (*sqlx.DB, error) {
	connStr := fmt.Sprintf("host=database port=5432 user=%s password=%s dbname=%s sslmode=disable",
		cfg.PostgresUser, cfg.PostgresPassword, cfg.PostgresDatabase)
	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		return nil, fmt.Errorf("error while open db connection: %w", err)
	}
	return db, nil
}
