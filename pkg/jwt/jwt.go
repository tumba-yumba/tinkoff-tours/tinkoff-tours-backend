package jwt

import (
	"fmt"
	"time"
	"tinkoff-tours/internal/user"

	"github.com/golang-jwt/jwt"
)

type TokenType string

const (
	ACCESS  TokenType = "access"
	REFRESH TokenType = "refresh"
)

type JWT struct {
	secret string
}

func New(secret string) *JWT {
	return &JWT{secret: secret}
}

func (s *JWT) GenerateToken(user *user.User, expireTime time.Duration, tokenType TokenType) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	claims["exp"] = time.Now().Add(expireTime).Unix()
	claims["id"] = user.Id
	claims["email"] = user.Email
	claims["role"] = user.Role
	claims["token_type"] = tokenType

	tokenString, err := token.SignedString([]byte(s.secret))
	if err != nil {
		return "", fmt.Errorf("error while generating tokens: %w", err)
	}
	return tokenString, nil
}

func (s *JWT) DecodeToken(jti string) (jwt.MapClaims, error) {
	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(jti, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(s.secret), nil
	})
	return claims, err
}

func (s *JWT) IsTokenExpired(jti string) (bool, error) {
	claims, err := s.DecodeToken(jti)
	if err != nil {
		if err.(*jwt.ValidationError).Errors == jwt.ValidationErrorExpired {
			return true, nil
		}
		return true, err
	}
	return claims["exp"].(float64) < float64(time.Now().Unix()), nil
}
