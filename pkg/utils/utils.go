package utils

import (
	"encoding/json"
	"io"
	"net/url"
	"tinkoff-tours/internal/common"

	"github.com/go-playground/validator/v10"
	"github.com/gorilla/schema"
)

var validate = validator.New()

func DecodeAndValidate[T any](body io.ReadCloser) (T, error) {
	var obj T
	if err := json.NewDecoder(body).Decode(&obj); err != nil {
		return obj, common.DecodeError{}
	}
	if err := ValidateStruct(obj); err != nil {
		return obj, err
	}
	return obj, nil
}

func ValidateStruct[T any](obj T) error {
	if err := validate.Struct(obj); err != nil {
		var validationErrors common.ValidationErrors
		for _, err := range err.(validator.ValidationErrors) {
			validationErrors.Errors = append(validationErrors.Errors, common.ValidationError{Field: err.Field(), Tag: err.Tag(), Value: err.Param()})
		}
		return validationErrors
	}
	return nil
}

func DecodeAndValidateQueryParam[T any](params url.Values) (T, error) {
	var obj T
	if err := schema.NewDecoder().Decode(&obj, params); err != nil {
		return obj, common.DecodeError{}
	}
	if err := ValidateStruct(obj); err != nil {
		return obj, err
	}
	return obj, nil
}
