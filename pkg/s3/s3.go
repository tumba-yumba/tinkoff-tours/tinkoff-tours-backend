package s3

import (
	"fmt"
	"mime/multipart"
	"path/filepath"
	"strings"
	"tinkoff-tours/configs"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"golang.org/x/exp/slices"
)

const MB = 1 << 20

var allowExtensions = []string{".jpg", ".png", ".jpeg"}

type S3 struct {
	s *session.Session

	bucketName string
}

func New(cfg *configs.Config) (*S3, error) {
	s, err := ConnectAWS(cfg)
	if err != nil {
		return nil, fmt.Errorf("error on s3 connection: %w", err)
	}
	return &S3{
		s:          s,
		bucketName: cfg.BucketName,
	}, nil
}

func (s *S3) UploadFile(filename string, file multipart.File) (string, error) {
	uploader := s3manager.NewUploader(s.s)
	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s.bucketName),
		Key:    aws.String(filename),
		Body:   file,
	})
	if err != nil {
		return "", fmt.Errorf("error while uploading file: %w", err)
	}
	return s.GetFilePath(filename), nil
}

func (s *S3) ValidatePhoto(fileHeader *multipart.FileHeader) error { //TODO: переместить в photo
	if fileHeader.Size > 10*MB {
		return PhotoTooLargeError{}
	}
	ext := strings.ToLower(filepath.Ext(fileHeader.Filename))
	if !slices.Contains(allowExtensions, ext) {
		return PhotoExtError{}
	}
	return nil
}

func (s *S3) GetFilePath(filename string) string {
	return fmt.Sprintf("https://storage.yandexcloud.net/%s/%s", s.bucketName, filename)
}
