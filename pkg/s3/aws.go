package s3

import (
	"tinkoff-tours/configs"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
)

func ConnectAWS(cfg *configs.Config) (*session.Session, error) {
	customResolver := func(service, region string, options ...func(*endpoints.Options)) (endpoints.ResolvedEndpoint, error) {
		if service == endpoints.S3ServiceID {
			return endpoints.ResolvedEndpoint{
				PartitionID:   "yc",
				URL:           "https://storage.yandexcloud.net",
				SigningRegion: "ru-central1",
			}, nil
		}
		return endpoints.DefaultResolver().EndpointFor(service, region, options...)
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
		Credentials: credentials.NewStaticCredentials(
			cfg.AWSAccessKeyId,
			cfg.AWSSecretAccessKey,
			"",
		),
		EndpointResolver: endpoints.ResolverFunc(customResolver),
	})
	return sess, err
}
