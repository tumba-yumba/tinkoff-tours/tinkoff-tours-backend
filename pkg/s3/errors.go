package s3

type PhotoExtError struct {
}

func (pee PhotoExtError) Error() string {
	return "Uploaded photo has not allowed extention"
}

type PhotoTooLargeError struct {
}

func (pe PhotoTooLargeError) Error() string {
	return "Uploaded photo too big"
}
