package responses

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
)

type SuccessOK struct {
	Success string `json:"success"`
}

func ResponseData(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func ResponseSuccess(w http.ResponseWriter) {
	ResponseData(w, http.StatusOK, map[string]string{"success": "true"})
}

func ResponseInternalError(w http.ResponseWriter, err error) {
	fmt.Println(err)
	ResponseData(w, http.StatusInternalServerError, map[string]string{"error": err.Error()})
}

func ResponseError(w http.ResponseWriter, code int, errorObj error) {
	errorType := reflect.TypeOf(errorObj).Name()

	payload := map[string]interface{}{"type": errorType}

	var decoded interface{}
	message := errorObj.Error() // TODO ValidationError почитать про `as` и `is`
	if err := json.Unmarshal([]byte(message), &decoded); err != nil {
		payload["error"] = message
	} else {
		payload["error"] = decoded
	}
	ResponseData(w, code, payload)
}
