package email

import (
	"crypto/tls"
	"fmt"
	"tinkoff-tours/configs"

	"gopkg.in/gomail.v2"
)

type Sender struct {
	*gomail.Dialer
}

func New(cfg *configs.Config) *Sender {
	d := gomail.NewDialer("smtp.yandex.ru", 465, cfg.Email, cfg.EmailPassword)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true} // TODO: in test without tls

	return &Sender{Dialer: d}
}

func (e *Sender) SendMessage(toEmail, subject, message string) error {
	m := gomail.NewMessage()

	m.SetHeader("From", e.Username)
	m.SetHeader("To", toEmail)
	m.SetHeader("Subject", subject)
	m.SetBody("text/plain", message)

	if err := e.DialAndSend(m); err != nil {
		return fmt.Errorf("error while send email \"%s\": %w", toEmail, err)
	}
	return nil
}
