package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"tinkoff-tours/configs"
	_ "tinkoff-tours/docs"
	"tinkoff-tours/internal"
	"tinkoff-tours/pkg/server"
)

// @title Tinkoff Tours API
// @version 0.1
// @description Tinkoff Tours API
// @BasePath /api

// @securitydefinitions.apikey BearerAuth
// @in header
// @name Authorization
// @description Put here "Bearer <your_jwt_token>"

// @query.collection.format multi
func main() {
	cfg, err := configs.Init()
	if err != nil {
		log.Fatal(err)
	}

	app, err := internal.New(cfg).Init()
	if err != nil {
		log.Fatal(err)
	}
	srv := server.New(cfg.Port, app.GetApiRouters())

	go func() {
		if err := srv.Start(); err != nil {
			log.Fatal(err)
		}
	}()

	log.Println("Server started at", srv.Addr)

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, os.Kill)
	<-quit

	log.Println("Server shutting down")

	if err := srv.Shutdown(context.Background()); err != nil {
		log.Printf("%v", err)
	}
}
