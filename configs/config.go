package configs

import (
	"github.com/spf13/viper"
)

type Config struct {
	Port int `mapstructure:"PORT"`

	PostgresUser     string `mapstructure:"POSTGRES_USER"`
	PostgresPassword string `mapstructure:"POSTGRES_PASSWORD"`
	PostgresDatabase string `mapstructure:"POSTGRES_DB"`

	JWTSecret string `mapstructure:"JWT_SECRET"`

	AccessTokenExpiresInMinute  int `mapstructure:"ACCESS_TOKEN_EXPIRES_IN_MINUTE"`
	RefreshTokenExpiresInMinute int `mapstructure:"REFRESH_TOKEN_EXPIRES_IN_MINUTE"`

	AWSAccessKeyId     string `mapstructure:"AWS_ACCESS_KEY_ID"`
	AWSSecretAccessKey string `mapstructure:"AWS_SECRET_ACCESS_KEY"`
	AWSRegion          string `mapstructure:"AWS_REGION"`
	BucketName         string `mapstructure:"BUCKET_NAME"`

	Email         string `mapstructure:"EMAIL"`
	EmailPassword string `mapstructure:"EMAIL_PASSWORD"`

	Url          string `mapstructure:"URL"`
	UnbookingUrl string `mapstructure:"UNBOOKING_URL"`
}

func Init() (*Config, error) {
	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	cfg := &Config{}
	if err := viper.Unmarshal(cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}
