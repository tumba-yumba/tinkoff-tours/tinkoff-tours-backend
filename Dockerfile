FROM golang:1.19-alpine AS build

WORKDIR /app

RUN go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

COPY go.mod go.sum ./

RUN go mod download

COPY ./ ./

RUN go build -o tours ./cmd/main.go
