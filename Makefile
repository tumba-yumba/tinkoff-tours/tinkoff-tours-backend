-include .env

DATABASE_URL=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@database:5432/${POSTGRES_DB}?sslmode=disable

all: build down migrate up

up:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose build

psql:
	docker exec -it tours_db psql -U postgres

migrate:
	docker-compose run --rm app migrate -path schema -database ${DATABASE_URL} -verbose up

# k - downgrade count
downgrade:
	docker-compose run --rm app migrate -path schema -database ${DATABASE_URL} -verbose down ${k}

# name - migration name
makemigration:
	docker-compose run --rm --volume=${PWD}/schema:/app/schema app migrate create -ext sql -dir schema -seq ${name}
	sudo chown -R ${USER} schema

maketests:
	go test ./...
	allure serve ./tests/allure-results/