package test

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func AuthClient(data url.Values, targetUrl string) map[string]interface{}{
	resp, err := http.PostForm(targetUrl, data) 
	if err != nil {
		log.Fatal(err)
	}
	var result map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func BookingsDeleteClient(data url.Values, targetUrl string, id string) map[string]interface{}{
	var body = strings.NewReader(data.Encode())
	resp, err := http.NewRequest(http.MethodDelete, targetUrl, body) 
	if err != nil {
		log.Fatal(err)
	}
	var result map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func BookingsGetClient(targetUrl string, id string) map[string]interface{}{
	resp, err := http.Get(targetUrl) 
	if err != nil {
		log.Fatal(err)
	}
	var result map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func BookingsPostClient(data url.Values, targetUrl string, id string) map[string]interface{}{
	resp, err := http.PostForm(targetUrl, data) 
	if err != nil {
		log.Fatal(err)
	}
	var result map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}