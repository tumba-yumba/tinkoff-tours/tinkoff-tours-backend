package test

import (
	"log"
	"net/url"
)

var baseUrl = "https://localhost/api"
var authUrl = baseUrl + "/auth"
var bookingsUrl = baseUrl + "/bookings"
var toursUrl = baseUrl + "/tourse"

func authUrlRegister()(string){
	var ur, er = url.JoinPath(authUrl, "register")
	if er != nil { log.Fatal(er) }
	return ur
}

func authUrlLogin()(string){
	var ur, er = url.JoinPath(authUrl, "login")
	if er != nil { log.Fatal(er) }
	return ur
}

func authUrlRefresh()(string){
	var ur, er = url.JoinPath(authUrl, "refresh")
	if er != nil { log.Fatal(er) }
	return ur
}

func bookingUrlUnbooking(id string)(string){
	var ur, er = url.JoinPath(bookingsUrl, id)
	if er != nil { log.Fatal(er) }
	return ur
}

func bookingUrlGetItems(tourse_id string, time_id string)(string){
	var ur, er = url.JoinPath(toursUrl, tourse_id, "times", time_id, "bookings")
	if er != nil { log.Fatal(er) }
	return ur
}

func bookingUrlBookingTour(tourse_id string, time_id string)(string){
	var ur, er = url.JoinPath(toursUrl, tourse_id, "times", time_id, "bookings")
	if er != nil { log.Fatal(er) }
	return ur
}

func bookingUrlGetCount(tourse_id string, time_id string)(string){
	var ur, er = url.JoinPath(toursUrl, tourse_id, "times", time_id, "bookings/count")
	if er != nil { log.Fatal(er) }
	return ur
}
