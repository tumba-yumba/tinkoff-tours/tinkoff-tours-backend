package test

import (
	"net/url"
	"testing"
	"github.com/ozontech/allure-go/pkg/framework/provider"
	"github.com/ozontech/allure-go/pkg/framework/suite"
)

type AuthSuite struct {
	suite.Suite
}

func (s *AuthSuite) TestAuthReggister(t provider.T) {
	data := url.Values{"email": {"bfdsdf@gmail.com"}, 
					"first_name": {"Eg"}, 
					"last_name": {"Eg"}, 
					"password": {"1234"}}
	result := AuthClient(data, authUrlRegister())
	t.Require().Equal(200, result["status_code"])
	t.Require().NotNil(result["access_token"])
}


func (s *AuthSuite) TestAuthLogin(t provider.T) {
	data := url.Values{"email": {"bfdsdf@gmail.com"}, "password": {"1234"}}
	result := AuthClient(data, authUrlLogin())
	t.Require().Equal(200, result["status_code"])
	t.Require().NotNil(result["access_token"])
}

// func (s *AuthSuite) TestAuthRefreshTocken(t provider.T) {
// 	dataLogin := url.Values{"email": {"bfdsdf@gmail.com"}, "password": {"1234"}}
// 	response := AuthClient(dataLogin, authUrlLogin())["refresh_token"]
// 	data := url.Values{"refresh_token": {response}}
// 	result := AuthClient(data, authUrlRefresh())
// 	t.Require().Equal(200, result["status_code"])
// 	t.Require().NotNil(result["access_token"])
// }


func AuthTestSuiteRunner(t *testing.T) {
	suite.RunSuite(t, new(AuthSuite))
}