package transport

import (
	"net/http"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/tour/service"
	"tinkoff-tours/internal/tour/transport/dto"
	"tinkoff-tours/internal/tour/transport/serializers"
	"tinkoff-tours/pkg/responses"
	"tinkoff-tours/pkg/s3"
	"tinkoff-tours/pkg/utils"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

type Handler struct {
	tourService *service.Service

	serializer *serializers.Serializer
}

func New(ts *service.Service, serializer *serializers.Serializer) *Handler {
	return &Handler{tourService: ts, serializer: serializer}
}

// @Summary Get tour
// @Description Get tour info
// @Tags tours
// @Produce json
// @Param id path string true "Tour ID"
// @Success 200 {object} dto.TourOut
// @Router /tours/{id} [get]
func (h *Handler) GetTour(w http.ResponseWriter, r *http.Request) {
	tourId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "path"})
		return
	}
	guideId := uuid.Nil
	if r.Header.Get("X-UserId") != "" {
		guideId, err = uuid.Parse(r.Header.Get("X-UserId"))
		if err != nil {
			responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
			return
		}
	}
	tour, err := h.tourService.GetTourById(tourId)
	if err != nil {
		switch err {
		case common.NotFoundError{Object: "Tour"}:
			responses.ResponseError(w, http.StatusNotFound, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, http.StatusOK, h.serializer.SerializeTour(tour, tour.GuideId == guideId))
}

// @Summary Delete tour
// @Description Delete tour
// @Tags tours
// @Produce json
// @Param id path string true "Tour ID"
// @Success 200 {object} responses.SuccessOK
// @Security BearerAuth
// @Router /tours/{id} [delete]
func (h *Handler) DeleteTour(w http.ResponseWriter, r *http.Request) {
	tourId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	err = h.tourService.DeleteTour(tourId)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseSuccess(w)
}

// @Summary Create tour
// @Description Create tour
// @Tags tours
// @Accept json
// @Produce json
// @Param tourData body dto.TourCreateIn true "Tour data"
// @Success 200 {object} dto.TourOut
// @Security BearerAuth
// @Router /tours [post]
func (h *Handler) CreateTour(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(r.Header.Get("X-UserId"))
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	data, err := utils.DecodeAndValidate[dto.TourCreateIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	tour, err := h.tourService.CreateTour(guideId, &data)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseData(w, http.StatusOK, h.serializer.SerializeTour(tour, true))
}

// @Summary Update tour
// @Description Update tour
// @Tags tours
// @Accept json
// @Produce json
// @Param id path string true "Tour ID"
// @Param tourData body dto.TourCreateIn true "Tour data"
// @Success 200 {object} responses.SuccessOK
// @Security BearerAuth
// @Router /tours/{id} [put]
func (h *Handler) UpdateTour(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(r.Header.Get("X-UserId"))
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	tourId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	data, err := utils.DecodeAndValidate[dto.TourCreateIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	if err = h.tourService.UpdateTour(tourId, guideId, &data); err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseSuccess(w)
}

// @Summary Upload photo
// @Description Upload photo
// @Tags tours
// @Accept mpfd
// @Produce json
// @Param photo formData file true "Photo File"
// @Success 200 {object} dto.PhotoOut
// @Security BearerAuth
// @Router /tours/photos [post]
func (h *Handler) UploadPhoto(w http.ResponseWriter, r *http.Request) {
	file, header, err := r.FormFile("photo")
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "photo"})
		return
	}
	id, path, err := h.tourService.UploadPhoto(header, file)
	if err != nil {
		switch err {
		case s3.PhotoExtError{}, s3.PhotoTooLargeError{}:
			responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, http.StatusOK, &dto.PhotoOut{Id: id.String(), Path: path})
}

// @Summary Delete photo
// @Description delete photo
// @Tags tours
// @Accept json
// @Produce json
// @Param photoData body dto.PhotoIn true "Photo Data"
// @Success 200 {object} responses.SuccessOK
// @Security BearerAuth
// @Router /tours/photos [delete]
func (h *Handler) DeletePhoto(w http.ResponseWriter, r *http.Request) {
	data, err := utils.DecodeAndValidate[dto.PhotoIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	if err := h.tourService.DeletePhoto(data.Id); err != nil {
		switch err {
		case common.NotFoundError{Object: "Tour"}:
			responses.ResponseError(w, http.StatusNotFound, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseSuccess(w)
}

// @Summary Get Tours
// @Description Get tours with filters and pagination
// @Tags tours
// @Produce json
// @Param filters query dto.Filters true "Filters and pagination"
// @Success 200 {object} dto.ToursListOut
// @Router /tours [get]
func (h *Handler) GetTours(w http.ResponseWriter, r *http.Request) {
	filters, err := utils.DecodeAndValidateQueryParam[dto.Filters](r.URL.Query())
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	toursCount, tours, err := h.tourService.GetTours(filters)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseData(w, http.StatusOK, h.serializer.SerializeToursList(tours, toursCount))
}
