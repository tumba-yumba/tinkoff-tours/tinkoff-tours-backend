package transport

import (
	"net/http"
	auth "tinkoff-tours/internal/auth"

	"github.com/gorilla/mux"
)

func AddApiRoutes(r *mux.Router, h *Handler, m auth.MiddlewareProvider) {
	t := r.PathPrefix("/tours").Subrouter()
	t.Handle("/{id}", m.Optional().AuthorizationMiddleware(http.HandlerFunc(h.GetTour))).Methods("GET")

	t.Handle("/{id}", m.AuthorizationMiddleware(http.HandlerFunc(h.UpdateTour))).Methods("PUT")
	t.Handle("/{id}", m.AuthorizationMiddleware(http.HandlerFunc(h.DeleteTour))).Methods("DELETE")
	t.Handle("", http.HandlerFunc(h.GetTours)).Methods("GET")
	t.Handle("", m.AuthorizationMiddleware(http.HandlerFunc(h.CreateTour))).Methods("POST")
	t.Handle("/photos", m.AuthorizationMiddleware(http.HandlerFunc(h.UploadPhoto))).Methods("POST")
	t.Handle("/photos", m.AuthorizationMiddleware(http.HandlerFunc(h.DeletePhoto))).Methods("DELETE")
}
