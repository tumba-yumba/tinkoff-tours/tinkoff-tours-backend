package dto

import (
	"tinkoff-tours/internal/common"

	"github.com/google/uuid"
)

type TourCreateIn struct {
	Title            string `json:"title" validate:"required" db:"title"`
	Description      string `json:"description" validate:"required" db:"description"`
	ShortDescription string `json:"short_description" validate:"required" db:"short_description"`
	EmailMessage     string `json:"email_message" validate:"required" db:"email_msg"`
	Country          string `json:"country" validate:"required" db:"country"`
	City             string `json:"city" validate:"required" db:"city"`

	Price           int `json:"price" validate:"required" db:"price"`
	MaxParticipants int `json:"max_participants" validate:"required" db:"max_participants"` // TODO: complex validation

	Times []TimeIn `json:"times" validate:"required"`

	Tags   []TagIn   `json:"tags"`
	Photos []PhotoIn `json:"photos"`
}

type TimeIn struct {
	StartTime string `json:"start_time" validate:"required" db:"start_time"`
	EndTime   string `json:"end_time" validate:"required" db:"end_time"`
}

type TagIn struct {
	Id string `json:"id" validate:"required"`
}

type PhotoIn struct {
	Id string `json:"id" validate:"required"`
}

type Filters struct {
	Country *string `schema:"country" db:"country" json:"country"`
	City    *string `schema:"city" db:"city" json:"city"`

	StartDate *string `schema:"start_date" db:"start_date" json:"start_date"`
	EndDate   *string `schema:"end_date" db:"end_date" json:"end_date"`

	StartTime *string `schema:"start_time" db:"start_time" json:"start_time"`
	EndTime   *string `schema:"end_time" db:"end_time" json:"end_time"`

	Participants *int `schema:"participants" db:"participants" json:"participants"`
	PriceLimit   *int `schema:"price_limit" db:"price_limit" json:"price_limit"`

	Tags []string `schema:"tags" db:"tags" json:"tags" collectionFormat:"multi"`

	*common.Pagination
}

type TourOut struct {
	Id               string `json:"id"`
	GuideId          string `json:"guide_id"`
	Title            string `json:"title"`
	Description      string `json:"description"`
	ShortDescription string `json:"short_description"`
	EmailMessage     string `json:"email_message"`
	Country          string `json:"country"`
	City             string `json:"region"`

	Price           int `json:"price"`
	MaxParticipants int `json:"max_participants"`

	Times  []TourTimeOut `json:"times"`
	Tags   []TourTagOut  `json:"tags"`
	Photos []PhotoOut    `json:"photos"`

	ModerateStatus string `json:"moderation_status"`
}

type TourTimeOut struct {
	Id        uuid.UUID `json:"id"`
	StartTime string    `json:"start_time"`
	EndTime   string    `json:"end_time"`
}

type TourTagOut struct {
	Id    string `json:"id"`
	Title string `json:"title"`
}

type PhotoOut struct {
	Id   string `json:"id"`
	Path string `json:"path"`
}

type ToursListOut struct {
	Count int             `json:"count"`
	Items []TourInListOut `json:"items"`
}

type TourInListOut struct {
	Id               string `json:"id"`
	Title            string `json:"title"`
	ShortDescription string `json:"short_description"`

	Country string `json:"country"`
	City    string `json:"city"`

	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`

	Price int `json:"price"`

	Photo string `json:"photo"`
}
