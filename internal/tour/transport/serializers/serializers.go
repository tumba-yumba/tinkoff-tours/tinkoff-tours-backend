package serializers

import (
	"strconv"
	"tinkoff-tours/internal/tag"
	"tinkoff-tours/internal/tour"
	"tinkoff-tours/internal/tour/transport/dto"
	"tinkoff-tours/pkg/s3"
)

type Serializer struct {
	s3 *s3.S3
}

func New(s3 *s3.S3) *Serializer {
	return &Serializer{s3: s3}
}

func (s *Serializer) SerializeTour(tour *tour.Tour, isGuide bool) *dto.TourOut {
	res := &dto.TourOut{
		Id:               tour.Id.String(),
		GuideId:          tour.GuideId.String(),
		Title:            tour.Title,
		Description:      tour.Description,
		ShortDescription: tour.ShortDescription,
		Country:          tour.Country,
		City:             tour.City,
		Price:            tour.Price,
		MaxParticipants:  tour.MaxParticipants,
		Times:            s.SerializeTourTimes(tour.Times),
		Tags:             s.SerializeTagsTitles(tour.Tags),
		Photos:           s.SerializePhoto(tour.Photos),
		ModerateStatus:   string(tour.ModerateStatus),
	}
	if isGuide {
		res.EmailMessage = tour.EmailMessage
	}
	return res
}

func (s *Serializer) SerializeTourTimes(times []tour.TourTime) []dto.TourTimeOut {
	result := make([]dto.TourTimeOut, 0)
	for _, time := range times {
		result = append(result, dto.TourTimeOut{
			Id:        time.Id,
			StartTime: time.StartTime.Format("2006-01-02 15:04"),
			EndTime:   time.EndTime.Format("2006-01-02 15:04"),
		})
	}
	return result
}
func (s *Serializer) SerializeTagsTitles(tags []tag.Tag) []dto.TourTagOut {
	result := make([]dto.TourTagOut, 0)
	for _, tag := range tags {
		result = append(result, dto.TourTagOut{Id: strconv.Itoa(tag.Id), Title: tag.Title})
	}
	return result
}

func (s *Serializer) SerializePhoto(photos []tour.TourPhotos) []dto.PhotoOut {
	result := make([]dto.PhotoOut, 0)
	for _, photo := range photos {
		result = append(result, dto.PhotoOut{Id: photo.Id.String(), Path: s.s3.GetFilePath(photo.Filename)})
	}
	return result
}

func (s *Serializer) SerializeToursList(tours []*tour.TourInList, toursCount int) dto.ToursListOut {
	result := make([]dto.TourInListOut, 0)
	for _, tour := range tours {
		result = append(result, dto.TourInListOut{
			Id:               tour.Id.String(),
			Title:            tour.Title,
			ShortDescription: tour.ShortDescription,
			Country:          tour.Country,
			City:             tour.City,
			StartTime:        tour.StartTime.Format("2006-01-02 15:04"),
			EndTime:          tour.EndTime.Format("2006-01-02 15:04"),
			Price:            tour.Price,
			Photo:            s.s3.GetFilePath(tour.Photo.String),
		})
	}
	return dto.ToursListOut{
		Count: toursCount,
		Items: result,
	}
}
