package service

import (
	"mime/multipart"
	"path/filepath"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/photo"
	"tinkoff-tours/internal/tour"
	"tinkoff-tours/internal/tour/transport/dto"

	"github.com/google/uuid"
)

type TourManager interface {
	GetTourById(id uuid.UUID) (*tour.Tour, error)
	GetToursByGuide(guideId uuid.UUID, p common.Pagination) ([]*tour.TourInList, error)
	GetTours(filters dto.Filters) ([]*tour.TourInList, error)
	GetToursCountWithFilters(filters dto.Filters) (int, error)
	GetToursCountByGuide(guideId uuid.UUID) (int, error)

	CreateTour(guideId uuid.UUID, data *dto.TourCreateIn) (uuid.UUID, error)
	UpdateTour(tourId, guideUd uuid.UUID, data *dto.TourCreateIn) error
	DeleteTour(tourId uuid.UUID) error
}

type PhotoManager interface {
	CreatePhoto(ext string) (*photo.Photo, error)
	DeletePhoto(id string) error
}

type S3Manager interface {
	ValidatePhoto(header *multipart.FileHeader) error
	UploadFile(filename string, file multipart.File) (string, error)
}

type Service struct {
	tourManager  TourManager
	photoManager PhotoManager
	s3Manager    S3Manager
}

func New(tm TourManager, pm PhotoManager, s3 S3Manager) *Service {
	return &Service{
		tourManager:  tm,
		photoManager: pm,
		s3Manager:    s3,
	}
}

func (s *Service) GetTourById(id uuid.UUID) (*tour.Tour, error) {
	return s.tourManager.GetTourById(id)
}

func (s *Service) DeleteTour(id uuid.UUID) error {
	return s.tourManager.DeleteTour(id)
}

func (s *Service) GetTours(filters dto.Filters) (int, []*tour.TourInList, error) {
	toursCount, err := s.tourManager.GetToursCountWithFilters(filters)
	if err != nil {
		return 0, nil, err
	}
	tours, err := s.tourManager.GetTours(filters)
	return toursCount, tours, err
}

func (s *Service) GetToursByGuide(guideId uuid.UUID, pagination common.Pagination) (int, []*tour.TourInList, error) {
	toursCount, err := s.tourManager.GetToursCountByGuide(guideId)
	if err != nil {
		return 0, nil, err
	}
	tours, err := s.tourManager.GetToursByGuide(guideId, pagination)
	return toursCount, tours, err
}

func (s *Service) UpdateTour(tourId, guideId uuid.UUID, data *dto.TourCreateIn) error {
	return s.tourManager.UpdateTour(tourId, guideId, data)
}

func (s *Service) CreateTour(guideId uuid.UUID, data *dto.TourCreateIn) (*tour.Tour, error) {
	id, err := s.tourManager.CreateTour(guideId, data)
	if err != nil {
		return nil, err
	}
	return s.GetTourById(id)
}

func (s *Service) UploadPhoto(fileHeader *multipart.FileHeader, file multipart.File) (uuid.UUID, string, error) {
	if err := s.s3Manager.ValidatePhoto(fileHeader); err != nil {
		return uuid.Nil, "", err
	}
	ext := filepath.Ext(fileHeader.Filename)
	photo, err := s.photoManager.CreatePhoto(ext)
	if err != nil {
		return uuid.Nil, "", err
	}
	filepath, err := s.s3Manager.UploadFile(photo.Filename, file)
	return photo.Id, filepath, err
}

func (s *Service) DeletePhoto(id string) error {
	return s.photoManager.DeletePhoto(id)
}
