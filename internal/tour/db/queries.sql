-- name: get-tour-by-id
SELECT *
FROM tours
WHERE id = $1;

-- name: get-tour-times-by-id
SELECT tt.id,
       tt.start_time,
       tt.end_time
FROM tours t
         JOIN tour_times tt ON t.id = tt.tour_id
WHERE t.id = $1;

-- name: get-tour-tags-by-id
SELECT tg.id, tg.title
FROM tours t
         JOIN tours_tags tt ON t.id = tt.tour_id
         JOIN tags tg ON tt.tag_id = tg.id
WHERE t.id = $1;

-- name: get-tour-photos-by-id
SELECT p.id, p.filename
FROM photos p
         JOIN tours t ON p.tour_id = t.id
WHERE p.tour_id = $1;

-- name: create-tour
INSERT INTO "tours" (id, guide_id, title, description, short_description, email_msg, country, city, price,
                     max_participants)
VALUES (:id, :guide_id, :title, :description, :short_description, :email_msg, :country, :city, :price,
        :max_participants);

-- name: add-times-to-tour
INSERT INTO tour_times (id, tour_id, start_time, end_time)
VALUES (:id, :tour_id, :start_time, :end_time);

-- name: add-tags-to-tour
INSERT INTO tours_tags (tour_id, tag_id)
VALUES (:tour_id, :tag_id);

-- name: add-photos-to-tour
UPDATE
    photos
SET tour_id = :tour_id
WHERE id IN (:photo_ids)
  AND tour_id IS NULL;

-- name: update-tour
UPDATE
    tours
SET title             = :title,
    description       = :description,
    short_description = :short_description,
    email_msg         = :email_msg,
    country           = :country,
    city              = :city,
    price             = :price,
    max_participants  = :max_participants,
    moderate_status   = 'pending'
WHERE id = :id;

-- name: clear-tour-tags
DELETE
FROM tours_tags tg
WHERE tg.tour_id = $1;

-- name: clear-tour-times
DELETE
FROM tour_times
WHERE tour_id = $1;

-- name: tours-list-by-filters-pagination
SELECT distinct on (t.id) t.id,
                          t.title,
                          t.short_description,
                          t.country,
                          t.city,
                          MIN(tti.start_time) as start_time,
                          tti.end_time,
                          t.price,
                          p.filename          AS photo
FROM public.tours t
         LEFT OUTER JOIN public.photos p ON t.id = p.tour_id
         LEFT OUTER JOIN public.tours_tags tt ON t.id = tt.tour_id
         LEFT OUTER JOIN public.tags tg ON tt.tag_id = tg.id
         LEFT OUTER JOIN public.tour_times tti ON t.id = tti.tour_id
WHERE (coalesce(:country) IS NULL
    OR t.country = :country)
  AND (coalesce(:city) IS NULL
    OR t.city = :city)
  AND (coalesce(:participants) IS NULL
    OR t.max_participants > :participants)
  AND (coalesce(:price_limit) IS NULL
    OR t.price < :price_limit)
  AND ('-1' IN (:tags)
    OR tg.id IN (:tags))
  AND (coalesce(:start_date) IS NULL
    OR cast(tti.start_time AS date) >= to_date(:start_date, 'YYYY-MM-DD'))
  AND (coalesce(:end_date) IS NULL
    OR cast(tti.end_time AS date) <= to_date(:end_date, 'YYYY-MM-DD'))
  AND (coalesce(:start_time) IS NULL
    OR cast(tti.start_time AS time) >= cast(to_timestamp(:start_time, 'HH24-MI') AS time))
  AND (coalesce(:end_time) IS NULL
    OR cast(tti.end_time AS time) <= cast(to_timestamp(:end_time, 'HH24-MI') AS time))
  AND tti.start_time >= now()
  AND moderate_status = 'approved'
GROUP BY t.id, tti.start_time, tti.end_time, p.filename
LIMIT :limit OFFSET :offset;

-- name: tours-count-by-filters
SELECT count(distinct t.id) AS tours_count
FROM public.tours t
         LEFT OUTER JOIN public.photos p ON t.id = p.tour_id
         LEFT OUTER JOIN public.tours_tags tt ON t.id = tt.tour_id
         LEFT OUTER JOIN public.tags tg ON tt.tag_id = tg.id
         LEFT OUTER JOIN public.tour_times tti ON t.id = tti.tour_id
WHERE (coalesce(:country) IS NULL
    OR t.country = :country)
  AND (coalesce(:city) IS NULL
    OR t.city = :city)
  AND (coalesce(:participants) IS NULL
    OR t.max_participants > :participants)
  AND (coalesce(:price_limit) IS NULL
    OR t.price < :price_limit)
  AND ('-1' IN (:tags)
    OR tg.id IN (:tags))
  AND (coalesce(:start_date) IS NULL
    OR cast(tti.start_time AS date) >= to_date(:start_date, 'YYYY-MM-DD'))
  AND (coalesce(:end_date) IS NULL
    OR cast(tti.end_time AS date) <= to_date(:end_date, 'YYYY-MM-DD'))
  AND (coalesce(:start_time) IS NULL
    OR cast(tti.start_time AS time) >= cast(to_timestamp(:start_time, 'HH24-MI') AS time))
  AND (coalesce(:end_time) IS NULL
    OR cast(tti.end_time AS time) <= cast(to_timestamp(:end_time, 'HH24-MI') AS time))
  AND tti.start_time >= now()
  AND moderate_status = 'approved';

-- name: tours-count-by-guide
SELECT count(DISTINCT t.id) AS tours_count
FROM public.tours t
         LEFT OUTER JOIN public.photos p ON t.id = p.tour_id
WHERE t.guide_id = $1;

-- name: tours-list-by-guide-pagination
SELECT distinct on (t.id) t.id,
       t.title,
       t.short_description,
       t.country,
       t.city,
       MIN(tti.start_time) as start_time,
       tti.end_time,
       t.price,
       p.filename AS photo
FROM tours t
         LEFT OUTER JOIN public.photos p ON t.id = p.tour_id
         LEFT JOIN public.tour_times tti ON t.id = tti.tour_id
WHERE t.guide_id = $1
GROUP BY t.id, tti.start_time, tti.end_time, p.filename
LIMIT $2 OFFSET $3;

-- name: change-moderation-status
UPDATE
    tours
SET moderate_status = $1
WHERE id = $2;

-- name: unmoderated-tours-count
SELECT count(*)
FROM tours
WHERE moderate_status = 'pending';

-- name: unmoderated-tours-list-pagination
SELECT distinct on (t.id) t.id,
       t.title,
       t.short_description,
       t.country,
       t.city,
       MIN(tti.start_time) as start_time,
       tti.end_time,
       t.price,
       p.filename AS photo
FROM tours t
    LEFT OUTER JOIN photos p ON t.id = p.tour_id
    LEFT JOIN public.tour_times tti ON t.id = tti.tour_id
WHERE moderate_status = 'pending'
GROUP BY t.id, tti.start_time, tti.end_time, p.filename
LIMIT $1 OFFSET $2;

-- name: delete-tour
DELETE
FROM tours
WHERE id = $1;

-- name: check-tour-time-exists
SELECT EXISTS(
               SELECT tt.id
               FROM tours t
                        JOIN tour_times tt ON t.id = tt.tour_id
               WHERE tt.tour_id = $1
                 AND tt.id = $2);

