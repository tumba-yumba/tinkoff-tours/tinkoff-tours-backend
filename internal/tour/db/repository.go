package db

import (
	"database/sql"
	"fmt"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/tour"
	"tinkoff-tours/internal/tour/transport/dto"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/nleof/goyesql"
)

type Repository struct {
	*sqlx.DB
	queries goyesql.Queries
}

func New(db *sqlx.DB) *Repository {
	return &Repository{
		db,
		goyesql.MustParseFile("internal/tour/db/queries.sql"),
	}
}

func (r *Repository) GetTourById(id uuid.UUID) (*tour.Tour, error) {
	var tour tour.Tour
	if err := r.Get(&tour, r.queries["get-tour-by-id"], id); err != nil {
		if err == sql.ErrNoRows {
			return nil, common.NotFoundError{Object: "Tour"}
		}
		return nil, fmt.Errorf("error while get tour: %w", err)
	}

	if err := r.Select(&tour.Times, r.queries["get-tour-times-by-id"], id); err != nil {
		return nil, fmt.Errorf("error while get tour times: %w", err)
	}
	if err := r.Select(&tour.Tags, r.queries["get-tour-tags-by-id"], id); err != nil {
		return nil, fmt.Errorf("error while get tour tags: %w", err)
	}
	if err := r.Select(&tour.Photos, r.queries["get-tour-photos-by-id"], id); err != nil {
		return nil, fmt.Errorf("error while get tour photos: %w", err)
	}
	return &tour, nil
}

func (r *Repository) DeleteTour(tourId uuid.UUID) error {
	res, err := r.Exec(r.queries["delete-tour"], tourId)
	if err != nil {
		return fmt.Errorf("error while delete tour: %w", err)
	}
	rowsCount, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("error while delete tour: %w", err)
	}
	if rowsCount != 1 {
		return common.NotFoundError{Object: "Tour"}
	}
	return nil
}

func (r *Repository) CreateTour(guideId uuid.UUID, data *dto.TourCreateIn) (uuid.UUID, error) {
	tx, err := r.Beginx()
	defer tx.Rollback()
	if err != nil {
		return uuid.Nil, fmt.Errorf("error while create transaction: %w", err)
	}

	id, err := uuid.NewUUID()
	if err != nil {
		return uuid.Nil, fmt.Errorf("error while create tour uuid: %w", err)
	}
	queryData := map[string]interface{}{
		"id":                id.String(),
		"guide_id":          guideId.String(),
		"title":             data.Title,
		"description":       data.Description,
		"short_description": data.ShortDescription,
		"email_msg":         data.EmailMessage,
		"country":           data.Country,
		"city":              data.City,
		"price":             data.Price,
		"max_participants":  data.MaxParticipants,
	}
	if _, err = tx.NamedExec(r.queries["create-tour"], queryData); err != nil {
		return uuid.Nil, fmt.Errorf("error while create tour: %w", err)
	}

	insertTimes, err := getInsertedTimes(id, data.Times)
	if err != nil {
		return uuid.Nil, fmt.Errorf("error while get inserted times: %w", err)
	}
	if _, err := tx.NamedExec(r.queries["add-times-to-tour"], insertTimes); err != nil {
		return uuid.Nil, fmt.Errorf("error while insert times in tour: %w", err)
	}

	insertTags := getInsertedTags(id, data.Tags)
	if len(insertTags) > 0 {
		if _, err := tx.NamedExec(r.queries["add-tags-to-tour"], insertTags); err != nil {
			return uuid.Nil, fmt.Errorf("error while bind tour with tags: %w", err)
		}
	}

	photoIds := make([]string, 0)
	for _, photo := range data.Photos {
		photoIds = append(photoIds, photo.Id)
	}
	if len(photoIds) > 0 {
		updatePhotosData := map[string]interface{}{"tour_id": id, "photo_ids": photoIds}
		query, args, err := r.prepareQueryWithNamedAndINParameters(r.queries["add-photos-to-tour"], updatePhotosData)
		if err != nil {
			return uuid.Nil, fmt.Errorf("error while prepare query: %w", err)
		}
		if _, err := tx.Exec(query, args...); err != nil {
			return uuid.Nil, fmt.Errorf("error while updated photos in create tour: %w", err)
		}
	}

	if err := tx.Commit(); err != nil {
		return uuid.Nil, fmt.Errorf("error while commit create tour: %w", err)
	}
	return id, nil
}

func (r *Repository) UpdateTour(tourId, guideId uuid.UUID, data *dto.TourCreateIn) error {
	tx, err := r.Beginx()
	defer tx.Rollback()
	if err != nil {
		return fmt.Errorf("error while create transaction: %w", err)
	}

	queryData := map[string]any{
		"id":                tourId,
		"guide_id":          guideId,
		"title":             data.Title,
		"description":       data.Description,
		"short_description": data.ShortDescription,
		"email_msg":         data.EmailMessage,
		"country":           data.Country,
		"city":              data.City,
		"price":             data.Price,
		"max_participants":  data.MaxParticipants,
	}
	if _, err = tx.NamedExec(r.queries["update-tour"], queryData); err != nil {
		return fmt.Errorf("error while create tour: %w", err)
	}

	insertTimes, err := getInsertedTimes(tourId, data.Times)
	if err != nil {
		return fmt.Errorf("error while get inserted times: %w", err)
	}
	if _, err = tx.Exec(r.queries["clear-tour-times"], tourId); err != nil {
		return fmt.Errorf("error while clear tour times: %w", err)
	}
	if _, err = tx.NamedExec(r.queries["add-times-to-tour"], insertTimes); err != nil {
		return fmt.Errorf("error while insert times in tour: %w", err)
	}

	insertTags := getInsertedTags(tourId, data.Tags)
	if len(insertTags) > 0 {
		if _, err = tx.Exec(r.queries["clear-tour-tags"], tourId); err != nil {
			return fmt.Errorf("error while clear tour tags: %w", err)
		}
		if _, err = tx.NamedExec(r.queries["add-tags-to-tour"], insertTags); err != nil {
			return fmt.Errorf("error while bind tour with tags: %w", err)
		}
	}

	photoIds := make([]string, 0)
	for _, photo := range data.Photos {
		photoIds = append(photoIds, photo.Id)
	}
	if len(photoIds) > 0 {
		updatePhotosData := map[string]interface{}{"tour_id": tourId, "photo_ids": photoIds}
		query, args, err := r.prepareQueryWithNamedAndINParameters(r.queries["add-photos-to-tour"], updatePhotosData)
		if err != nil {
			return fmt.Errorf("error while prepare query: %w", err)
		}

		if _, err := tx.Exec(query, args...); err != nil {
			return fmt.Errorf("error while updated photos in create tour: %w", err)
		}
	}
	if err := tx.Commit(); err != nil {
		return fmt.Errorf("error while commit create tour: %w", err)
	}
	return nil
}

func (r *Repository) GetTours(filters dto.Filters) ([]*tour.TourInList, error) {
	if len(filters.Tags) == 0 {
		filters.Tags = append(filters.Tags, "-1")
	}
	query, args, err := r.prepareQueryWithNamedAndINParameters(r.queries["tours-list-by-filters-pagination"], filters)
	if err != nil {
		return nil, fmt.Errorf("error while prepare query for get tours: %w", err)
	}
	var tours []*tour.TourInList
	if err := r.Select(&tours, query, args...); err != nil {
		return nil, fmt.Errorf("error while get filtered tours: %w", err)
	}
	return tours, nil
}

func (r *Repository) GetToursCountWithFilters(filters dto.Filters) (int, error) {
	if len(filters.Tags) == 0 {
		filters.Tags = append(filters.Tags, "-1")
	}
	query, args, err := r.prepareQueryWithNamedAndINParameters(r.queries["tours-count-by-filters"], filters)
	if err != nil {
		return 0, fmt.Errorf("error while prepare query for get tours: %w", err)
	}
	var toursCount int
	if err := r.Get(&toursCount, query, args...); err != nil {
		return 0, fmt.Errorf("error while get tours count: %w", err)
	}
	return toursCount, nil
}

func (r *Repository) GetToursByGuide(guideId uuid.UUID, pagination common.Pagination) ([]*tour.TourInList, error) {
	var tours []*tour.TourInList
	if err := r.Select(&tours, r.queries["tours-list-by-guide-pagination"],
		guideId, pagination.Limit, pagination.Offset); err != nil {
		return nil, fmt.Errorf("error while get tours by guide: %w", err)
	}
	return tours, nil
}

func (r *Repository) GetToursCountByGuide(guideId uuid.UUID) (int, error) {
	var count int
	err := r.Get(&count, r.queries["tours-count-by-guide"], guideId)
	if err != nil {
		return 0, fmt.Errorf("error while get tours count by guide: %w", err)
	}
	return count, nil
}

func (r *Repository) IsGuideTour(tourId, guideId uuid.UUID) (bool, error) {
	var id uuid.UUID
	err := r.QueryRowx("SELECT id FROM tours WHERE id = $1 AND guide_id = $2", tourId, guideId).Scan(&id)
	if err == sql.ErrNoRows {
		return false, nil
	}
	if err != nil {
		return false, fmt.Errorf("error while check is guide tour")
	}
	return true, nil
}

func (r *Repository) ChangeModerationStatus(tourId uuid.UUID, status string) error {
	_, err := r.Exec(r.queries["change-moderation-status"], status, tourId)
	if err != nil {
		return fmt.Errorf("error while change tour moderation status: %w", err)
	}
	return nil
}

func (r *Repository) prepareQueryWithNamedAndINParameters(queryRaw string, parameters interface{}) (string, []interface{}, error) {
	query, args, err := sqlx.Named(queryRaw, parameters)
	if err != nil {
		return "", nil, err
	}
	query, args, err = sqlx.In(query, args...)
	if err != nil {
		return "", nil, err
	}
	query = r.Rebind(query)
	return query, args, err
}

func (r *Repository) GetUnmoderatedToursCount() (int, error) {
	var count int
	err := r.Get(&count, r.queries["unmoderated-tours-count"])
	if err != nil {
		return 0, fmt.Errorf("error while get unmoderated tours count: %w", err)
	}
	return count, nil
}

func (r *Repository) GetUnmoderatedTours(pagination common.Pagination) ([]*tour.TourInList, error) {
	var tours []*tour.TourInList
	if err := r.Select(&tours, r.queries["unmoderated-tours-list-pagination"], pagination.Limit, pagination.Offset); err != nil {
		return nil, fmt.Errorf("error while get unmoderated tours: %w", err)
	}
	return tours, nil
}

func (r *Repository) IsTourTimeExists(tourId, timeId uuid.UUID) (bool, error) {
	var check bool
	if err := r.Get(&check, r.queries["check-tour-time-exists"], tourId.String(), timeId.String()); err != nil {
		return false, fmt.Errorf("error while check is tour time exists: %w", err)
	}
	return check, nil
}

func getInsertedTimes(tourId uuid.UUID, times []dto.TimeIn) ([]map[string]any, error) {
	insertTimes := make([]map[string]interface{}, 0)
	for _, time := range times {
		timeId, err := uuid.NewUUID()
		if err != nil {
			return nil, fmt.Errorf("error while create uuid for tour time: %w", err)
		}
		insertTimes = append(insertTimes, map[string]interface{}{
			"id":         timeId.String(),
			"tour_id":    tourId,
			"start_time": time.StartTime,
			"end_time":   time.EndTime})
	}
	return insertTimes, nil
}

func getInsertedTags(tourId uuid.UUID, tags []dto.TagIn) []map[string]any {
	insertTags := make([]map[string]any, 0)
	for _, tag := range tags {
		insertTags = append(insertTags, map[string]interface{}{"tour_id": tourId, "tag_id": tag.Id})
	}
	return insertTags
}
