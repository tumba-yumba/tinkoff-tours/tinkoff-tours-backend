package api

import (
	"tinkoff-tours/internal/auth"
	photoRepository "tinkoff-tours/internal/photo/db"
	tourRepository "tinkoff-tours/internal/tour/db"
	tourService "tinkoff-tours/internal/tour/service"
	tourHandler "tinkoff-tours/internal/tour/transport"
	"tinkoff-tours/internal/tour/transport/serializers"
	userRepository "tinkoff-tours/internal/user/db"
	userService "tinkoff-tours/internal/user/service"
	"tinkoff-tours/pkg/jwt"
	"tinkoff-tours/pkg/s3"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

func Configure(d *sqlx.DB, r *mux.Router, s *s3.S3, j *jwt.JWT) {
	ts := tourService.New(
		tourRepository.New(d),
		photoRepository.New(d),
		s,
	)
	us := userService.New(
		userRepository.New(d),
	)
	ser := serializers.New(s)
	tourHandler.AddApiRoutes(r, tourHandler.New(ts, ser), auth.NewMiddlewareProvider(us, j))
}
