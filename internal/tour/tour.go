package tour

import (
	"database/sql"
	"time"
	"tinkoff-tours/internal/moderation"
	"tinkoff-tours/internal/tag"

	"github.com/google/uuid"
)

type Tour struct {
	Id      uuid.UUID `db:"id"`
	GuideId uuid.UUID `db:"guide_id"`

	Title            string `db:"title"`
	Description      string `db:"description"`
	ShortDescription string `db:"short_description"`
	EmailMessage     string `db:"email_msg"`
	Country          string `db:"country"`
	City             string `db:"city"`

	Price           int `db:"price"`
	MaxParticipants int `db:"max_participants"`

	Times []TourTime

	Tags   []tag.Tag    `db:"tags"`
	Photos []TourPhotos `db:"photos"`

	ModerateStatus moderation.Status `db:"moderate_status"`
}

type TourTime struct {
	Id        uuid.UUID `db:"id"`
	StartTime time.Time `db:"start_time"`
	EndTime   time.Time `db:"end_time"`
}

type TourPhotos struct {
	Id       uuid.UUID `db:"id"`
	Filename string    `db:"filename"`
}

type TourInList struct {
	Id uuid.UUID `db:"id"`

	Title            string `db:"title"`
	ShortDescription string `db:"short_description"`
	Country          string `db:"country"`
	City             string `db:"city"`

	Price           int `db:"price"`
	MaxParticipants int `db:"max_participants"`

	StartTime time.Time `db:"start_time"`
	EndTime   time.Time `db:"end_time"`

	Photo sql.NullString `db:"photo"`

	ModerateStatus moderation.Status `db:"moderate_status"`
}
