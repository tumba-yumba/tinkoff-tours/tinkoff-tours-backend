package token

import user "tinkoff-tours/internal/user"

type IssuedToken struct {
	JTI       string `db:"jti"`
	IsRevoked bool   `db:"is_revoked"`
	*user.User
}
