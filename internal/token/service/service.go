package service

import (
	"time"
	"tinkoff-tours/internal/auth/transport/dto"
	"tinkoff-tours/internal/token"
	"tinkoff-tours/internal/user"
	"tinkoff-tours/pkg/jwt"
)

type TokenManager interface {
	GetIssuedToken(jti string) (*token.IssuedToken, error)

	RevokeIssuedToken(token *token.IssuedToken) error
	RevokeAllTokensOfUser(user *user.User) error
	CreateIssuedToken(jti string, user *user.User) error
}

type JWTGenerator interface {
	GenerateToken(user *user.User, expireTime time.Duration, tokenType jwt.TokenType) (string, error)
}

type Service struct {
	tokenManager TokenManager
	jwtGenerator JWTGenerator

	accessExpireTime  int
	refreshExpireTime int
}

func New(tm TokenManager, j JWTGenerator, accessExpireTime, refreshExpireTime int) *Service {
	return &Service{
		tokenManager:      tm,
		jwtGenerator:      j,
		accessExpireTime:  accessExpireTime,
		refreshExpireTime: refreshExpireTime,
	}
}

func (s *Service) GetIssuedToken(jti string) (*token.IssuedToken, error) {
	return s.tokenManager.GetIssuedToken(jti)
}

func (s *Service) RevokeIssuedToken(issuedToken *token.IssuedToken) error {
	return s.tokenManager.RevokeIssuedToken(issuedToken)
}

func (s *Service) RevokeAllTokensOfUser(user *user.User) error {
	return s.tokenManager.RevokeAllTokensOfUser(user)
}

func (s *Service) GenerateAccessRefreshToken(user *user.User) (*dto.TokensOut, error) {
	accessToken, err := s.jwtGenerator.GenerateToken(user, time.Duration(s.accessExpireTime)*time.Minute, jwt.ACCESS)
	if err != nil {
		return nil, err
	}
	refreshToken, err := s.jwtGenerator.GenerateToken(user, time.Duration(s.refreshExpireTime)*time.Minute, jwt.REFRESH)

	if err != nil {
		return nil, err
	}
	err = s.tokenManager.CreateIssuedToken(refreshToken, user)
	if err != nil {
		return nil, err
	}
	return &dto.TokensOut{AccessToken: accessToken, RefreshToken: refreshToken}, nil
}
