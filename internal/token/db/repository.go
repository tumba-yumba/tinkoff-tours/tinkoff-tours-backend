package db

import (
	"fmt"

	"tinkoff-tours/internal/token"
	"tinkoff-tours/internal/user"

	"github.com/jmoiron/sqlx"
	"github.com/nleof/goyesql"
)

type Repository struct {
	*sqlx.DB
	queries goyesql.Queries
}

func New(db *sqlx.DB) *Repository {
	return &Repository{
		db,
		goyesql.MustParseFile("internal/token/db/queries.sql"),
	}
}

func (r *Repository) CreateIssuedToken(jti string, u *user.User) error {
	_, err := r.Exec(r.queries["create-issued-token"], jti, u.Id)
	if err != nil {
		return fmt.Errorf("error while create token: %w", err)
	}
	return nil
}

func (r *Repository) GetIssuedToken(jti string) (*token.IssuedToken, error) {
	var t token.IssuedToken
	err := r.Unsafe().QueryRowx(r.queries["get-issued-token"], jti).StructScan(&t)
	if err != nil {
		return nil, fmt.Errorf("error while get issued token: %w", err)
	}
	return &t, nil
}

func (r *Repository) RevokeIssuedToken(issuedToken *token.IssuedToken) error {
	_, err := r.Exec(r.queries["revoke-issued-token"], issuedToken.JTI)
	if err != nil {
		return fmt.Errorf("error while revoke issued token: %w", err)
	}
	return nil
}

func (r *Repository) RevokeAllTokensOfUser(user *user.User) error {
	_, err := r.Exec(r.queries["revoke-all-issued-tokens-by-user"], user.Id)
	if err != nil {
		return fmt.Errorf("error while revoke all tokens of user: %w", err)
	}
	return nil
}
