-- name: create-issued-token
INSERT INTO issued_tokens (jti, user_id)
VALUES ($1, $2);

-- name: get-issued-token
SELECT *
FROM issued_tokens it
         JOIN users u ON it.user_id = u.id
WHERE it.jti = $1;

-- name: revoke-issued-token
UPDATE
    issued_tokens
SET is_revoked = TRUE
WHERE jti = $1;

-- name: revoke-all-issued-tokens-by-user
UPDATE
    issued_tokens
SET is_revoked = TRUE
WHERE user_id = $1
