-- name: can-bookings
SELECT t.max_participants >= coalesce((SELECT sum(tb.participants_count)
                                       FROM tour_booking tb
                                       WHERE tb.tour_id = $2
                                         and tb.tour_time_id = $3), 0) + $1
FROM tours t
WHERE t.id = $2;

-- name: bookings-tour
INSERT INTO tour_booking (id, tour_id, tour_time_id, email, name, participants_count)
VALUES (:id, :tour_id, :tour_time_id, :email, :name, :participants_count);

-- name: unbooking-tour
DELETE
FROM tour_booking
WHERE id = $1;

-- name: get-bookings-participants-count
SELECT coalesce(sum(tb.participants_count), 0)
FROM tour_booking tb
WHERE tb.tour_id = $1
  AND tb.tour_time_id = $2;

-- name: get-bookings-list
SELECT *
FROM tour_booking
WHERE tour_id = $1
  AND tour_time_id = $2;

-- name: get-bookings-list-count
SELECT coalesce(count(tb.id), 0)
FROM tour_booking tb
WHERE tb.tour_id = $1
  AND tb.tour_time_id = $2;

-- name: get-finished-bookings
UPDATE tour_booking tb SET is_email_send = true
FROM tour_times tt
WHERE tt.id = tb.tour_time_id and tt.end_time < now() + INTERVAL '5 hours' AND is_email_send = false
RETURNING tb.id, tb.tour_id, tb.tour_time_id, tb.name, tb.email, tb.participants_count;

