package db

import (
	"database/sql"
	"fmt"
	"tinkoff-tours/internal/booking"
	"tinkoff-tours/internal/booking/transport/dto"
	"tinkoff-tours/internal/common"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/nleof/goyesql"
)

type Repository struct {
	*sqlx.DB
	queries goyesql.Queries
}

func New(d *sqlx.DB) *Repository {
	return &Repository{
		DB:      d,
		queries: goyesql.MustParseFile("internal/booking/db/queries.sql"),
	}
}

func (r *Repository) CanBooking(tourId, timeId uuid.UUID, credentials *dto.BookingCredentials) (bool, error) {
	var canBooking bool
	if err := r.Get(&canBooking, r.queries["can-bookings"], credentials.ParticipantsCount, tourId, timeId); err != nil {
		if err == sql.ErrNoRows {
			return false, common.NotFoundError{Object: "Tour"}
		}
		return false, fmt.Errorf("error while check can bookings: %w", err)
	}
	return canBooking, nil
}

func (r *Repository) BookingTour(tourId, timeId uuid.UUID, credentials *dto.BookingCredentials) (uuid.UUID, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return uuid.Nil, fmt.Errorf("error while generate id for tour bookings: %w", err)
	}
	dataIn := map[string]interface{}{
		"id":                 id,
		"tour_id":            tourId,
		"tour_time_id":       timeId,
		"email":              credentials.Email,
		"name":               credentials.Name,
		"participants_count": credentials.ParticipantsCount,
	}
	if _, err := r.NamedExec(r.queries["bookings-tour"], dataIn); err != nil {
		return uuid.Nil, fmt.Errorf("error while create tour bookings: %w", err)
	}
	return id, nil
}

func (r *Repository) UnbookingTour(bookingId uuid.UUID) error {
	res, err := r.Exec(r.queries["unbooking-tour"], bookingId)
	if err != nil {
		return fmt.Errorf("error while delete tour bookings: %w", err)
	}
	rowsCount, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("error while delete tour bookings: %w", err)
	}
	if rowsCount != 1 {
		return common.NotFoundError{Object: "Tour bookings"}
	}
	return nil
}

func (r *Repository) GetBookingParticipantsCount(tourId, timeId uuid.UUID) (count int, err error) {
	if err := r.Get(&count, r.queries["get-bookings-participants-count"], tourId, timeId); err != nil {
		return 0, fmt.Errorf("error while get bookings count: %w", err)
	}
	return count, nil
}

func (r *Repository) GetBookingItems(tourId, timeId uuid.UUID, pagination common.Pagination) ([]*booking.Booking, error) {
	var bookingItems []*booking.Booking
	if err := r.Unsafe().Select(&bookingItems, r.queries["get-bookings-list"], tourId, timeId); err != nil {
		return nil, fmt.Errorf("error while get bookings items: %w", err)
	}
	return bookingItems, nil
}

func (r *Repository) GetBookingCount(tourId, timeId uuid.UUID) (int, error) {
	var count int
	if err := r.Get(&count, r.queries["get-bookings-list-count"], tourId, timeId); err != nil {
		return 0, fmt.Errorf("error while get bookings list count: %w", err)
	}
	return count, nil
}

func (r *Repository) GetFinishedBookings() ([]*booking.Booking, error) {
	var bookings []*booking.Booking
	if err := r.Unsafe().Select(&bookings, r.queries["get-finished-bookings"]); err != nil {
		return nil, fmt.Errorf("error while get finished bookings: %w", err)
	}
	return bookings, nil
}
