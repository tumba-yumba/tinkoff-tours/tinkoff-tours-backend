package booking

import (
	"errors"

	"github.com/google/uuid"
)

type Booking struct {
	Id                uuid.UUID `db:"id"`
	TourId            uuid.UUID `db:"tour_id"`
	TourTimeId        uuid.UUID `db:"tour_time_id"`
	Name              string    `db:"name"`
	Email             string    `db:"email"`
	ParticipantsCount int       `db:"participants_count"`
}

var ErrorParticipantsLimitExceeded = errors.New("participants limit exceeded")
