package api

import (
	"log"
	"time"
	"tinkoff-tours/internal/auth"
	bookingRepository "tinkoff-tours/internal/booking/db"
	bookingService "tinkoff-tours/internal/booking/service"
	bookingHandler "tinkoff-tours/internal/booking/transport"
	tourRepository "tinkoff-tours/internal/tour/db"
	userRepository "tinkoff-tours/internal/user/db"
	userService "tinkoff-tours/internal/user/service"
	"tinkoff-tours/pkg/email"
	"tinkoff-tours/pkg/jwt"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

func Configure(db *sqlx.DB, r *mux.Router, e *email.Sender, js *jwt.JWT, url, unbookingUrl string) {
	bs := bookingService.New(
		bookingRepository.New(db),
		tourRepository.New(db),
		e,
		url,
		unbookingUrl,
	)
	us := userService.New(
		userRepository.New(db),
	)
	log.Printf("hello\n")
	StartFeedbackSender(1*time.Minute, bs)

	bookingHandler.AddApiRoutes(r, bookingHandler.New(bs), auth.NewMiddlewareProvider(us, js))
}

func StartFeedbackSender(minutes time.Duration, bs *bookingService.Service) chan bool {
	ticker := time.NewTicker(minutes)
	quit := make(chan bool, 1)
	go func() {
		for {
			select {
			case <-ticker.C:
				err := bs.SendFeedbackRequests()
				if err != nil {
					log.Printf("error while send feedback request: %v\n", err)
				}
			case <-quit:
				ticker.Stop()
			}
		}
	}()
	return quit
}
