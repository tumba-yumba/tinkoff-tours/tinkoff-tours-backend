package dto

type BookingCredentials struct {
	Name              string `json:"name" validation:"required"`
	Email             string `json:"email" validation:"email,required"`
	ParticipantsCount int    `json:"participants_count" validate:"gt=0"`
}

type BookingCountOut struct {
	Count int `json:"count"`
}

type BookingListOut struct {
	Count int                `json:"count"`
	Items []BookingInListOut `json:"items"`
}

type BookingInListOut struct {
	Name              string `json:"name"`
	Email             string `json:"email"`
	ParticipantsCount int    `json:"participants_count"`
}
