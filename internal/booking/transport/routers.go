package transport

import (
	"net/http"
	"tinkoff-tours/internal/auth"

	"github.com/gorilla/mux"
)

func AddApiRoutes(r *mux.Router, h *Handler, m auth.MiddlewareProvider) {
	b := r.PathPrefix("/bookings").Subrouter()
	b.HandleFunc("/{id}", h.UnbookingTour).Methods("DELETE")

	t := r.PathPrefix("/tours").Subrouter()
	t.Handle("/{tour_id}/times/{time_id}/bookings", m.AuthorizationMiddleware(http.HandlerFunc(h.GetBookingItems))).Methods("GET")
	t.HandleFunc("/{tour_id}/times/{time_id}/bookings", h.BookingTour).Methods("POST")
	t.HandleFunc("/{tour_id}/times/{time_id}/bookings/count", h.GetBookingCount).Methods("GET")
}
