package transport

import (
	"net/http"
	"tinkoff-tours/internal/booking"
	bookingService "tinkoff-tours/internal/booking/service"
	"tinkoff-tours/internal/booking/transport/dto"
	bookingDto "tinkoff-tours/internal/booking/transport/dto"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/pkg/responses"
	"tinkoff-tours/pkg/utils"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

type Handler struct {
	bookingService *bookingService.Service
}

func New(bs *bookingService.Service) *Handler {
	return &Handler{
		bookingService: bs,
	}
}

// BookingTour
// @Summary Booking tour
// @Description Booking tour, send email notification
// @Tags bookings
// @Accept json
// @Produce json
// @Param tour_id path string true "Tour ID"
// @Param time_id path string true "Time ID"
// @Param credentials body dto.BookingCredentials true "Booking Credentials"
// @Success 200 {object} responses.SuccessOK
// @Router /tours/{tour_id}/times/{time_id}/bookings [post]
func (h *Handler) BookingTour(w http.ResponseWriter, r *http.Request) {
	tourId, err := uuid.Parse(mux.Vars(r)["tour_id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "tour id"})
		return
	}
	timeId, err := uuid.Parse(mux.Vars(r)["time_id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "time id"})
		return
	}
	credentials, err := utils.DecodeAndValidate[dto.BookingCredentials](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	err = h.bookingService.BookingTour(tourId, timeId, &credentials)
	if err != nil {
		switch err {
		case common.NotFoundError{Object: "Tour"}:
			responses.ResponseError(w, http.StatusNotFound, err)
		case booking.ErrorParticipantsLimitExceeded:
			responses.ResponseError(w, http.StatusBadRequest, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseSuccess(w)
}

// UnbookingTour
// @Summary Unbooking tour
// @Description Unbooking tour, send email notification
// @Tags bookings
// @Produce json
// @Param id path string true "Booking ID"
// @Success 200 {object} responses.SuccessOK
// @Router /bookings/{id} [delete]
func (h *Handler) UnbookingTour(w http.ResponseWriter, r *http.Request) {
	bookingId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "bookings-id"})
		return
	}
	if err := h.bookingService.UnbookingTour(bookingId); err != nil {
		switch err {
		case common.NotFoundError{Object: "Tour bookings"}:
			responses.ResponseError(w, http.StatusNotFound, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseSuccess(w)
}

// GetBookingCount
// @Summary Get bookings count in tour
// @Description Get bookings count in tour
// @Tags bookings
// @Produce json
// @Param tour_id path string true "Tour ID"
// @Param time_id path string true "Time ID"
// @Success 200 {object} dto.BookingCountOut
// @Router /tours/{tour_id}/times/{time_id}/bookings/count [get]
func (h *Handler) GetBookingCount(w http.ResponseWriter, r *http.Request) {
	tourId, err := uuid.Parse(mux.Vars(r)["tour_id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "tour id"})
		return
	}
	timeId, err := uuid.Parse(mux.Vars(r)["time_id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "time id"})
		return
	}
	count, err := h.bookingService.GetBookingParticipantsCount(tourId, timeId)
	if err != nil {
		responses.ResponseInternalError(w, err)
	}
	responses.ResponseData(w, http.StatusOK, bookingDto.BookingCountOut{Count: count})
}

// GetBookingItems
// @Summary Get bookings items of tour
// @Description Get bookings items of tour
// @Tags bookings
// @Produce json
// @Param tour_id path string true "Tour ID"
// @Param time_id path string true "Time ID"
// @Param pagination query common.Pagination true "Pagination"
// @Success 200 {object} dto.BookingListOut
// @Security BearerAuth
// @Router /tours/{tour_id}/times/{time_id}/bookings [get]
func (h *Handler) GetBookingItems(w http.ResponseWriter, r *http.Request) {
	tourId, err := uuid.Parse(mux.Vars(r)["tour_id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "tour id"})
		return
	}
	timeId, err := uuid.Parse(mux.Vars(r)["time_id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "tour id"})
		return
	}
	guideId, err := uuid.Parse(r.Header.Get("X-UserId"))
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "guide id"})
		return
	}
	pagination, err := utils.DecodeAndValidateQueryParam[common.Pagination](r.URL.Query())
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	count, bookingItems, err := h.bookingService.GetBookingItems(tourId, timeId, guideId, pagination)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseData(w, http.StatusOK, SerializeBookingItems(bookingItems, count))
}

func SerializeBookingItems(bookings []*booking.Booking, bookingCount int) bookingDto.BookingListOut {
	result := make([]bookingDto.BookingInListOut, 0)
	for _, b := range bookings {
		result = append(result, dto.BookingInListOut{
			Name:              b.Name,
			Email:             b.Email,
			ParticipantsCount: b.ParticipantsCount,
		})
	}
	return dto.BookingListOut{
		Count: bookingCount,
		Items: result,
	}
}
