package service

import (
	"fmt"
	"tinkoff-tours/internal/auth"
	"tinkoff-tours/internal/booking"
	"tinkoff-tours/internal/booking/transport/dto"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/tour"

	"github.com/google/uuid"
)

type BookingManager interface {
	BookingTour(tourId, timeId uuid.UUID, cred *dto.BookingCredentials) (uuid.UUID, error)
	CanBooking(tourId, timeId uuid.UUID, credentials2 *dto.BookingCredentials) (bool, error)
	UnbookingTour(id uuid.UUID) error
	GetBookingParticipantsCount(tourId, timeId uuid.UUID) (int, error)
	GetBookingCount(tourId, timeId uuid.UUID) (int, error)
	GetBookingItems(tourId, timeId uuid.UUID, p common.Pagination) ([]*booking.Booking, error)
	GetFinishedBookings() ([]*booking.Booking, error)
}

type TourManager interface {
	GetTourById(id uuid.UUID) (*tour.Tour, error)
	IsTourTimeExists(tourId, tourTimeId uuid.UUID) (bool, error)
	IsGuideTour(tourId, guideId uuid.UUID) (bool, error)
}

type EmailSender interface {
	SendMessage(email, subject, msg string) error
}

type Service struct {
	bookingManager BookingManager
	tourManager    TourManager
	emailSender    EmailSender

	url          string
	unbookingUrl string
}

const bookingSubject = "Бронирование Тура"
const bookingMessage = "Вы были забронированы на тур %s/tours/%s.\n\n%s\n\nЧтобы отменить бронирование перейдите по ссылке: %s/%s"

const feedbackRequestSubject = "Отзыв на экскурсию"
const feedbackRequestMessage = "Вы недавно посещали экскурсию %s/tours/%s. Оставьте пожалуйста отзыв %s/tours/%s?review=%s"

func New(bm BookingManager, tm TourManager, e EmailSender, url, unbookingUrl string) *Service {
	return &Service{
		bookingManager: bm,
		tourManager:    tm,

		emailSender: e,

		url:          url,
		unbookingUrl: unbookingUrl,
	}
}

func (s *Service) BookingTour(tourId, timeId uuid.UUID, credentials *dto.BookingCredentials) error {
	if err := s.CheckCanBooking(tourId, timeId, credentials); err != nil {
		return err
	}
	id, err := s.bookingManager.BookingTour(tourId, timeId, credentials)
	if err != nil {
		return err
	}
	tour, err := s.tourManager.GetTourById(tourId)
	if err != nil {
		return err
	}
	msg := fmt.Sprintf(bookingMessage, s.url, tourId, tour.EmailMessage, s.unbookingUrl, id.String())
	return s.emailSender.SendMessage(credentials.Email, bookingSubject, msg)
}

func (s *Service) CheckCanBooking(tourId, timeId uuid.UUID, credentials *dto.BookingCredentials) error {
	canBooking, err := s.bookingManager.CanBooking(tourId, timeId, credentials)
	if err != nil {
		return err
	}
	if !canBooking {
		return booking.ErrorParticipantsLimitExceeded
	}
	timeExists, err := s.tourManager.IsTourTimeExists(tourId, timeId)
	if err != nil {
		return err
	}
	if !timeExists {
		return common.NotFoundError{Object: "TourTime"}
	}
	return nil
}

func (s *Service) UnbookingTour(bookingId uuid.UUID) error {
	if err := s.bookingManager.UnbookingTour(bookingId); err != nil {
		return err
	}
	return nil
}

func (s *Service) GetBookingParticipantsCount(tourId, timeId uuid.UUID) (int, error) {
	return s.bookingManager.GetBookingParticipantsCount(tourId, timeId)
}

func (s *Service) GetBookingItems(tourId, timeId, guideId uuid.UUID, pagination common.Pagination) (int, []*booking.Booking, error) {
	check, err := s.tourManager.IsGuideTour(tourId, guideId)
	if err != nil {
		return 0, nil, err
	}
	if !check {
		return 0, nil, auth.ForbiddenError
	}
	count, err := s.bookingManager.GetBookingCount(tourId, timeId)
	if err != nil {
		return 0, nil, err
	}
	bookingItems, err := s.bookingManager.GetBookingItems(tourId, timeId, pagination)
	return count, bookingItems, err
}

func (s *Service) SendFeedbackRequests() error {
	bookings, err := s.bookingManager.GetFinishedBookings()
	if err != nil {
		return err
	}
	for _, booking := range bookings {
		msg := fmt.Sprintf(feedbackRequestMessage, s.url, booking.TourId, s.url, booking.TourId, booking.Id)
		if err := s.emailSender.SendMessage(booking.Email, feedbackRequestSubject, msg); err != nil {
			return err
		}
	}
	return nil
}
