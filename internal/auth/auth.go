package auth

import "errors"

var UnauthorizedError = errors.New("unauthorized, please check your email or password")

var TokenExpiredError = errors.New("your tokens has expired")

var TokenRevokedError = errors.New("your tokens has revoked")

var ForbiddenError = errors.New("forbidden")
