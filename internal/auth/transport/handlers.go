package transport

import (
	"net/http"
	"tinkoff-tours/internal/auth"
	"tinkoff-tours/internal/auth/service"
	"tinkoff-tours/internal/auth/transport/dto"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/pkg/responses"
	"tinkoff-tours/pkg/utils"
)

type Handler struct {
	authService *service.Service
}

func New(as *service.Service) *Handler {
	return &Handler{
		authService: as,
	}
}

// Register
// @Summary Registration user
// @Description Registration user
// @Tags auth
// @Accept json
// @Produce json
// @Param credentials body dto.RegistrationIn true "User credentials"
// @Success 200 {object} dto.TokensOut
// @Router /auth/register [post]
func (h *Handler) Register(w http.ResponseWriter, r *http.Request) {
	credentials, err := utils.DecodeAndValidate[dto.RegistrationIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	tokenOut, err := h.authService.RegisterGuide(&credentials)
	if err != nil {
		switch err {
		case common.AlreadyExistsError{Object: "Guide"}:
			responses.ResponseError(w, http.StatusBadRequest, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, http.StatusOK, tokenOut)
}

// Login
// @Summary Login User
// @Description Login user
// @Tags auth
// @Accept json
// @Produce json
// @Param credentials body dto.LoginIn true "User credentials"
// @Success 200 {object} dto.TokensOut
// @Router /auth/login [post]
func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	credentials, err := utils.DecodeAndValidate[dto.LoginIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusBadRequest, err) // TODO: поправить комент
		return
	}
	tokensOut, err := h.authService.Login(&credentials)
	if err != nil {
		switch err {
		case common.NotFoundError{Object: "User"}:
			responses.ResponseError(w, http.StatusNotFound, err)
		case auth.UnauthorizedError:
			responses.ResponseError(w, http.StatusUnauthorized, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, http.StatusOK, tokensOut)
}

// Refresh
// @Summary Refresh Token
// @Description Refresh tokens
// @Tags auth
// @Accept json
// @Produce json
// @Param credentials body dto.TokenIn true "Refresh tokens"
// @Success 200 {object} dto.TokensOut
// @Router /auth/refresh [post]
func (h *Handler) Refresh(w http.ResponseWriter, r *http.Request) {
	tokenIn, err := utils.DecodeAndValidate[dto.TokenIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusBadRequest, err) // TODO: поправить комент
		return
	}
	tokensOut, err := h.authService.Refresh(&tokenIn)
	if err != nil {
		switch err {
		case auth.TokenRevokedError, auth.TokenExpiredError:
			responses.ResponseError(w, http.StatusUnauthorized, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, http.StatusOK, tokensOut)
}
