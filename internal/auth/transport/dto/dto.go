package dto

type LoginIn struct {
	Email    string `json:"email" validate:"email,required"`
	Password string `json:"password" validate:"required"`
}

type RegistrationIn struct {
	FirstName string `json:"first_name" validate:"required"`
	LastName  string `json:"last_name" validate:"required"`
	LoginIn
}

type TokenIn struct {
	RefreshToken string `json:"refresh_token" validate:"jwt,required"`
}

type TokensOut struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}
