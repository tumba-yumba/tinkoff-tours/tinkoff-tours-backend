package transport

import "github.com/gorilla/mux"

func AddApiRouter(r *mux.Router, h *Handler) {
	authRouter := r.PathPrefix("/auth").Subrouter()
	authRouter.HandleFunc("/register", h.Register).Methods("POST")
	authRouter.HandleFunc("/login", h.Login).Methods("POST")
	authRouter.HandleFunc("/refresh", h.Refresh).Methods("POST")
}
