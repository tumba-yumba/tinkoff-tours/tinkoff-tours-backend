package api

import (
	authService "tinkoff-tours/internal/auth/service"
	authHandler "tinkoff-tours/internal/auth/transport"
	tokenRepository "tinkoff-tours/internal/token/db"
	tokenService "tinkoff-tours/internal/token/service"
	userRepository "tinkoff-tours/internal/user/db"
	userService "tinkoff-tours/internal/user/service"
	"tinkoff-tours/pkg/jwt"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

func Configure(db *sqlx.DB, r *mux.Router, js *jwt.JWT, accessExpireTime, refreshExpireTime int) {
	ts := tokenService.New(tokenRepository.New(db), js, accessExpireTime, refreshExpireTime)
	us := userService.New(userRepository.New(db))
	authHandler.AddApiRouter(r, authHandler.New(authService.New(ts, us, js)))
}
