package service

import (
	"tinkoff-tours/internal/auth"
	"tinkoff-tours/internal/auth/transport/dto"
	"tinkoff-tours/internal/token"
	"tinkoff-tours/internal/user"
	"tinkoff-tours/pkg/passwords"
)

type TokenManager interface {
	GenerateAccessRefreshToken(u *user.User) (*dto.TokensOut, error)
	GetIssuedToken(jti string) (*token.IssuedToken, error)
	RevokeAllTokensOfUser(u *user.User) error
	RevokeIssuedToken(issuedToken *token.IssuedToken) error
}

type UserManager interface {
	CreateUser(cred *dto.RegistrationIn, role user.Role) (*user.User, error)
	GetUserByEmail(email string) (*user.User, error)
}

type JWTExpireChecker interface {
	IsTokenExpired(jti string) (bool, error)
}

type Service struct {
	tokenManager     TokenManager
	userManager      UserManager
	jwtExpireChecker JWTExpireChecker
}

func New(tm TokenManager, um UserManager, j JWTExpireChecker) *Service {
	return &Service{
		tokenManager:     tm,
		userManager:      um,
		jwtExpireChecker: j,
	}
}

func (s *Service) RegisterGuide(credentials *dto.RegistrationIn) (*dto.TokensOut, error) {
	u, err := s.userManager.CreateUser(credentials, user.GuideRole)
	if err != nil {
		return nil, err
	}
	tokensOut, err := s.tokenManager.GenerateAccessRefreshToken(u)
	if err != nil {
		return nil, err
	}
	return tokensOut, nil
}

func (s *Service) Login(credentials *dto.LoginIn) (*dto.TokensOut, error) {
	u, err := s.userManager.GetUserByEmail(credentials.Email)
	if err != nil {
		return nil, err
	}

	if err = passwords.CheckPasswordHash(credentials.Password, u.Password); err != nil {
		return nil, auth.UnauthorizedError
	}
	tokensOut, err := s.tokenManager.GenerateAccessRefreshToken(u)
	if err != nil {
		return nil, err
	}
	return tokensOut, nil
}

func (s *Service) Refresh(tokenIn *dto.TokenIn) (*dto.TokensOut, error) {
	issuedToken, err := s.tokenManager.GetIssuedToken(tokenIn.RefreshToken)
	if err != nil {
		return nil, err
	}

	if issuedToken.IsRevoked {
		if err := s.tokenManager.RevokeAllTokensOfUser(issuedToken.User); err != nil {
			return nil, err
		}
		return nil, auth.TokenRevokedError
	}

	isExpired, err := s.jwtExpireChecker.IsTokenExpired(issuedToken.JTI)
	if isExpired {
		if err != nil {
			return nil, err
		}
		return nil, auth.TokenExpiredError
	}

	if err := s.tokenManager.RevokeIssuedToken(issuedToken); err != nil {
		return nil, err
	}

	tokensOut, err := s.tokenManager.GenerateAccessRefreshToken(issuedToken.User)
	if err != nil {
		return nil, err
	}
	return tokensOut, nil
}
