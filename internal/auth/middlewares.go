package auth

import (
	"net/http"
	"strings"
	"tinkoff-tours/internal/user"
	"tinkoff-tours/pkg/jwt"

	jwtUtils "github.com/golang-jwt/jwt"
	"github.com/google/uuid"
)

type UserProvider interface {
	GetUserById(id uuid.UUID) (*user.User, error)
}

type JWTDecoder interface {
	DecodeToken(jti string) (jwtUtils.MapClaims, error)
}

type MiddlewareProvider struct {
	userProvider UserProvider
	jwtDecoder   JWTDecoder

	required bool
}

func NewMiddlewareProvider(up UserProvider, jd JWTDecoder) MiddlewareProvider {
	return MiddlewareProvider{
		userProvider: up,
		jwtDecoder:   jd,
		required:     true,
	}
}

func (m MiddlewareProvider) Optional() MiddlewareProvider {
	m.required = false
	return m
}

func (m MiddlewareProvider) AuthorizationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		if len(token) == 0 {
			if m.required {
				http.Error(w, "Missing Authorization Header", http.StatusUnauthorized)
			} else {
				next.ServeHTTP(w, r)
			}
			return
		}
		token = strings.Replace(token, "Bearer ", "", 1)
		claims, err := m.jwtDecoder.DecodeToken(token)
		if err != nil {
			if err.(*jwtUtils.ValidationError).Errors == jwtUtils.ValidationErrorExpired {
				http.Error(w, "Token Expired", http.StatusUnauthorized)
				return
			}
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		if claims["token_type"] != string(jwt.ACCESS) {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}

		guideId, err := uuid.Parse(claims["id"].(string))
		if err != nil {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		u, err := m.userProvider.GetUserById(guideId)
		if err != nil {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}

		r.Header.Set("X-UserId", u.Id.String())
		r.Header.Set("X-Email", u.Email)
		r.Header.Set("X-Role", string(u.Role))
		next.ServeHTTP(w, r)
	})
}

func (m MiddlewareProvider) ModerationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		role := r.Header.Get("X-Role")
		if role != string(user.ModeratorRole) {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		next.ServeHTTP(w, r)
	})
}
