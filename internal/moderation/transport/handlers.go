package transport

import (
	"net/http"
	"tinkoff-tours/internal/common"
	guideSerializer "tinkoff-tours/internal/guide/transport/serializers"
	moderationService "tinkoff-tours/internal/moderation/service"
	moderateDto "tinkoff-tours/internal/moderation/transport/dto"
	tourSerializer "tinkoff-tours/internal/tour/transport/serializers"
	"tinkoff-tours/pkg/responses"
	"tinkoff-tours/pkg/utils"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

type Handler struct {
	moderationService *moderationService.Service

	tourSerializer  *tourSerializer.Serializer
	guideSerializer *guideSerializer.Serializer
}

func New(ms *moderationService.Service, gser *guideSerializer.Serializer, tser *tourSerializer.Serializer) *Handler {
	return &Handler{
		moderationService: ms,

		guideSerializer: gser,
		tourSerializer:  tser,
	}
}

// @Summary Moderate guide
// @Description Moderate guide
// @Tags moderation
// @Produce json
// @Param id path string true "Guide ID"
// @Param moderateIn body dto.ModerateIn true "Moderate guide"
// @Success 200 {object} responses.SuccessOK
// @Security BearerAuth
// @Router /moderation/guides/{id} [post]
func (h *Handler) ModerateGuide(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	moderateIn, err := utils.DecodeAndValidate[moderateDto.ModerateIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	err = h.moderationService.ModerateGuide(guideId, moderateIn.Status)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseSuccess(w)
}

// @Summary Get unmoderated guides
// @Description Get unmoderated guides
// @Tags moderation
// @Produce json
// @Param pagination query common.Pagination true "Pagination"
// @Success 200 {object} dto.GuidesListOut
// @Security BearerAuth
// @Router /moderation/guides [get]
func (h *Handler) GetUnmoderatedGuides(w http.ResponseWriter, r *http.Request) {
	pagination, err := utils.DecodeAndValidateQueryParam[common.Pagination](r.URL.Query())
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	guides, count, err := h.moderationService.GetUnmoderatedGuides(pagination)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseData(w, http.StatusOK, h.guideSerializer.SerializeGuidesList(guides, count))
}

// @Summary Moderate tour
// @Description Moderate tour
// @Tags moderation
// @Produce json
// @Param id path string true "Tour ID"
// @Param moderateIn body dto.ModerateIn true "Moderate tour"
// @Success 200 {object} responses.SuccessOK
// @Security BearerAuth
// @Router /moderation/tours/{id} [post]
func (h *Handler) ModerateTour(w http.ResponseWriter, r *http.Request) {
	tourId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	moderateIn, err := utils.DecodeAndValidate[moderateDto.ModerateIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	err = h.moderationService.ModerateTour(tourId, moderateIn.Status)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseSuccess(w)
}

// @Summary Get unmoderated tours
// @Description Get unmoderated tours
// @Tags moderation
// @Produce json
// @Param pagination query common.Pagination true "Pagination"
// @Success 200 {object} dto.ToursListOut
// @Security BearerAuth
// @Router /moderation/tours [get]
func (h *Handler) GetUnmoderatedTours(w http.ResponseWriter, r *http.Request) {
	pagination, err := utils.DecodeAndValidateQueryParam[common.Pagination](r.URL.Query())
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	tours, count, err := h.moderationService.GetUnmoderatedTours(pagination)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseData(w, http.StatusOK, h.tourSerializer.SerializeToursList(tours, count))
}
