package dto

type ModerateIn struct {
	Status  string `json:"status" validate:"required,oneof=approved rejected"`
	Comment string `json:"comment"`
}
