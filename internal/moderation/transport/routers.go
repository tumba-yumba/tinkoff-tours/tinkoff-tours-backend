package transport

import (
	"tinkoff-tours/internal/auth"

	"github.com/gorilla/mux"
)

func AddApiRoutes(r *mux.Router, h *Handler, mp auth.MiddlewareProvider) {
	m := r.PathPrefix("/moderation").Subrouter()
	m.Use(mp.AuthorizationMiddleware, mp.ModerationMiddleware)
	m.HandleFunc("/guides/{id}", h.ModerateGuide).Methods("POST")
	m.HandleFunc("/guides", h.GetUnmoderatedGuides).Methods("GET")
	m.HandleFunc("/tours/{id}", h.ModerateTour).Methods("POST")
	m.HandleFunc("/tours", h.GetUnmoderatedTours).Methods("GET")
}
