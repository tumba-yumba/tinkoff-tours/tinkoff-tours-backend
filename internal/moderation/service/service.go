package service

import (
	"github.com/google/uuid"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/guide"
	"tinkoff-tours/internal/tour"
)

//go:generate mockgen -source=service.go -destination=mocks/service.go
type UnmoderatedGuidesManager interface {
	ChangeModerationStatus(guideId uuid.UUID, status string) error
	GetUnmoderatedGuidesCount() (int, error)
	GetUnmoderatedGuides(p common.Pagination) ([]*guide.Guide, error)
}

type UnmoderatedToursManager interface {
	ChangeModerationStatus(tourId uuid.UUID, status string) error
	GetUnmoderatedToursCount() (int, error)
	GetUnmoderatedTours(p common.Pagination) ([]*tour.TourInList, error)
}

type Service struct {
	unmoderatedGuidesManager UnmoderatedGuidesManager
	unmoderatedToursManager  UnmoderatedToursManager
}

func New(ugm UnmoderatedGuidesManager, utm UnmoderatedToursManager) *Service {
	return &Service{
		unmoderatedGuidesManager: ugm,
		unmoderatedToursManager:  utm,
	}
}

func (s *Service) ModerateGuide(guideId uuid.UUID, status string) error {
	return s.unmoderatedGuidesManager.ChangeModerationStatus(guideId, status)
}

func (s *Service) GetUnmoderatedGuides(pagination common.Pagination) (guides []*guide.Guide, count int, err error) {
	guidesCount, err := s.unmoderatedGuidesManager.GetUnmoderatedGuidesCount()
	if err != nil {
		return nil, 0, err
	}
	guides, err = s.unmoderatedGuidesManager.GetUnmoderatedGuides(pagination)
	return guides, guidesCount, err
}

func (s *Service) ModerateTour(tourId uuid.UUID, status string) error {
	return s.unmoderatedToursManager.ChangeModerationStatus(tourId, status)
}

func (s *Service) GetUnmoderatedTours(pagination common.Pagination) (tours []*tour.TourInList, count int, err error) {
	toursCount, err := s.unmoderatedToursManager.GetUnmoderatedToursCount()
	if err != nil {
		return nil, 0, err
	}
	tours, err = s.unmoderatedToursManager.GetUnmoderatedTours(pagination)
	return tours, toursCount, err
}
