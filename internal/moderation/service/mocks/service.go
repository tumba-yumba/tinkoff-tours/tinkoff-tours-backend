// Code generated by MockGen. DO NOT EDIT.
// Source: service.go

// Package mock_service is a generated GoMock package.
package mock_service

import (
	reflect "reflect"
	common "tinkoff-tours/internal/common"
	guide "tinkoff-tours/internal/guide"
	tour "tinkoff-tours/internal/tour"

	gomock "github.com/golang/mock/gomock"
	uuid "github.com/google/uuid"
)

// MockUnmoderatedGuidesManager is a mock of UnmoderatedGuidesManager interface.
type MockUnmoderatedGuidesManager struct {
	ctrl     *gomock.Controller
	recorder *MockUnmoderatedGuidesManagerMockRecorder
}

// MockUnmoderatedGuidesManagerMockRecorder is the mock recorder for MockUnmoderatedGuidesManager.
type MockUnmoderatedGuidesManagerMockRecorder struct {
	mock *MockUnmoderatedGuidesManager
}

// NewMockUnmoderatedGuidesManager creates a new mock instance.
func NewMockUnmoderatedGuidesManager(ctrl *gomock.Controller) *MockUnmoderatedGuidesManager {
	mock := &MockUnmoderatedGuidesManager{ctrl: ctrl}
	mock.recorder = &MockUnmoderatedGuidesManagerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockUnmoderatedGuidesManager) EXPECT() *MockUnmoderatedGuidesManagerMockRecorder {
	return m.recorder
}

// ChangeModerationStatus mocks base method.
func (m *MockUnmoderatedGuidesManager) ChangeModerationStatus(guideId uuid.UUID, status string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ChangeModerationStatus", guideId, status)
	ret0, _ := ret[0].(error)
	return ret0
}

// ChangeModerationStatus indicates an expected call of ChangeModerationStatus.
func (mr *MockUnmoderatedGuidesManagerMockRecorder) ChangeModerationStatus(guideId, status interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ChangeModerationStatus", reflect.TypeOf((*MockUnmoderatedGuidesManager)(nil).ChangeModerationStatus), guideId, status)
}

// GetUnmoderatedGuides mocks base method.
func (m *MockUnmoderatedGuidesManager) GetUnmoderatedGuides(p common.Pagination) ([]*guide.Guide, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetUnmoderatedGuides", p)
	ret0, _ := ret[0].([]*guide.Guide)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetUnmoderatedGuides indicates an expected call of GetUnmoderatedGuides.
func (mr *MockUnmoderatedGuidesManagerMockRecorder) GetUnmoderatedGuides(p interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetUnmoderatedGuides", reflect.TypeOf((*MockUnmoderatedGuidesManager)(nil).GetUnmoderatedGuides), p)
}

// GetUnmoderatedGuidesCount mocks base method.
func (m *MockUnmoderatedGuidesManager) GetUnmoderatedGuidesCount() (int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetUnmoderatedGuidesCount")
	ret0, _ := ret[0].(int)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetUnmoderatedGuidesCount indicates an expected call of GetUnmoderatedGuidesCount.
func (mr *MockUnmoderatedGuidesManagerMockRecorder) GetUnmoderatedGuidesCount() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetUnmoderatedGuidesCount", reflect.TypeOf((*MockUnmoderatedGuidesManager)(nil).GetUnmoderatedGuidesCount))
}

// MockUnmoderatedToursManager is a mock of UnmoderatedToursManager interface.
type MockUnmoderatedToursManager struct {
	ctrl     *gomock.Controller
	recorder *MockUnmoderatedToursManagerMockRecorder
}

// MockUnmoderatedToursManagerMockRecorder is the mock recorder for MockUnmoderatedToursManager.
type MockUnmoderatedToursManagerMockRecorder struct {
	mock *MockUnmoderatedToursManager
}

// NewMockUnmoderatedToursManager creates a new mock instance.
func NewMockUnmoderatedToursManager(ctrl *gomock.Controller) *MockUnmoderatedToursManager {
	mock := &MockUnmoderatedToursManager{ctrl: ctrl}
	mock.recorder = &MockUnmoderatedToursManagerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockUnmoderatedToursManager) EXPECT() *MockUnmoderatedToursManagerMockRecorder {
	return m.recorder
}

// ChangeModerationStatus mocks base method.
func (m *MockUnmoderatedToursManager) ChangeModerationStatus(tourId uuid.UUID, status string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ChangeModerationStatus", tourId, status)
	ret0, _ := ret[0].(error)
	return ret0
}

// ChangeModerationStatus indicates an expected call of ChangeModerationStatus.
func (mr *MockUnmoderatedToursManagerMockRecorder) ChangeModerationStatus(tourId, status interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ChangeModerationStatus", reflect.TypeOf((*MockUnmoderatedToursManager)(nil).ChangeModerationStatus), tourId, status)
}

// GetUnmoderatedTours mocks base method.
func (m *MockUnmoderatedToursManager) GetUnmoderatedTours(p common.Pagination) ([]*tour.TourInList, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetUnmoderatedTours", p)
	ret0, _ := ret[0].([]*tour.TourInList)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetUnmoderatedTours indicates an expected call of GetUnmoderatedTours.
func (mr *MockUnmoderatedToursManagerMockRecorder) GetUnmoderatedTours(p interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetUnmoderatedTours", reflect.TypeOf((*MockUnmoderatedToursManager)(nil).GetUnmoderatedTours), p)
}

// GetUnmoderatedToursCount mocks base method.
func (m *MockUnmoderatedToursManager) GetUnmoderatedToursCount() (int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetUnmoderatedToursCount")
	ret0, _ := ret[0].(int)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetUnmoderatedToursCount indicates an expected call of GetUnmoderatedToursCount.
func (mr *MockUnmoderatedToursManagerMockRecorder) GetUnmoderatedToursCount() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetUnmoderatedToursCount", reflect.TypeOf((*MockUnmoderatedToursManager)(nil).GetUnmoderatedToursCount))
}
