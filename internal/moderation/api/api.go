package moderation

import (
	"tinkoff-tours/internal/auth"
	guideRepository "tinkoff-tours/internal/guide/db"
	guideSerializer "tinkoff-tours/internal/guide/transport/serializers"
	moderationService "tinkoff-tours/internal/moderation/service"
	moderationHandler "tinkoff-tours/internal/moderation/transport"
	tourRepository "tinkoff-tours/internal/tour/db"
	tourSerializers "tinkoff-tours/internal/tour/transport/serializers"
	userRepository "tinkoff-tours/internal/user/db"
	userService "tinkoff-tours/internal/user/service"
	"tinkoff-tours/pkg/jwt"
	"tinkoff-tours/pkg/s3"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

func Configure(d *sqlx.DB, r *mux.Router, s *s3.S3, j *jwt.JWT) {
	ms := moderationService.New(
		guideRepository.New(d),
		tourRepository.New(d),
	)
	us := userService.New(userRepository.New(d))
	moderationHandler.AddApiRoutes(
		r,
		moderationHandler.New(ms, guideSerializer.New(s), tourSerializers.New(s)),
		auth.NewMiddlewareProvider(us, j),
	)
}
