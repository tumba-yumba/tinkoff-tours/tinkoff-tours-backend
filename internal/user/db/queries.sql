-- name: create-user
INSERT INTO users (id, email, password_hash, ROLE, first_name, last_name)
VALUES (:id, :email, :password_hash, :role, :first_name, :last_name)
RETURNING
    *;

-- name: check-user-exists
SELECT EXISTS(
               SELECT 1
               FROM users
               WHERE email = $1);

-- name: get-user-by-id
SELECT *
FROM users
WHERE id = $1;

-- name: get-user-by-email
SELECT *
FROM users
WHERE email = $1;

