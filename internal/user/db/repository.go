package db

import (
	"database/sql"
	"fmt"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/user"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/nleof/goyesql"
)

type Repository struct {
	*sqlx.DB
	queries goyesql.Queries
}

func New(db *sqlx.DB) *Repository {
	return &Repository{
		db,
		goyesql.MustParseFile("internal/user/db/queries.sql"),
	}
}

func (ur *Repository) CreateUser(email, passwordHash, firstName, lastName string, role user.Role) (*user.User, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return nil, fmt.Errorf("error while create guide uuid: %w", err)
	}
	data := map[string]interface{}{
		"id":            id.String(),
		"email":         email,
		"password_hash": passwordHash,
		"first_name":    firstName,
		"last_name":     lastName,
		"role":          role,
	}
	nstmt, err := ur.PrepareNamed(ur.queries["create-user"])
	if err != nil {
		return nil, fmt.Errorf("error prepare stmt while create user: %w", err)
	}
	var user user.User
	if err := nstmt.Unsafe().Get(&user, data); err != nil {
		return nil, fmt.Errorf("error while create user: %w", err)
	}
	return &user, nil
}

func (ur *Repository) CheckUserExists(email string) (bool, error) {
	var exists bool
	if err := ur.Get(&exists, ur.queries["check-user-exists"], email); err != nil {
		return false, fmt.Errorf("error while check user exists: %w", err)
	}
	return exists, nil
}

func (ur *Repository) GetUserById(id uuid.UUID) (*user.User, error) {
	var user user.User
	if err := ur.Unsafe().Get(&user, ur.queries["get-user-by-id"], id); err != nil {
		if err == sql.ErrNoRows {
			return nil, common.NotFoundError{Object: "User"}
		}
		return nil, fmt.Errorf("error while get user by id: %w", err)
	}
	return &user, nil
}

func (ur *Repository) GetUserByEmail(email string) (*user.User, error) {
	var user user.User
	if err := ur.Unsafe().Get(&user, ur.queries["get-user-by-email"], email); err != nil {
		if err == sql.ErrNoRows {
			return nil, common.NotFoundError{Object: "User"}
		}
		return nil, fmt.Errorf("error while get user by email: %w", err)
	}
	return &user, nil
}
