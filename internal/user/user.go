package user

import (
	"time"

	"github.com/google/uuid"
)

type Role string

const (
	GuideRole     Role = "guide"
	ModeratorRole Role = "admin"
)

type User struct {
	Id        uuid.UUID `db:"id"`
	Role      Role      `db:"role"`
	FirstName string    `db:"first_name"`
	LastName  string    `db:"last_name"`
	Email     string    `db:"email"`
	Password  string    `db:"password_hash"`
	CreatedAt time.Time `db:"created_at"`
}
