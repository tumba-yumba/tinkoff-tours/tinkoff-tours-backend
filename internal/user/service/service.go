package service

import (
	"tinkoff-tours/internal/auth/transport/dto"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/user"
	"tinkoff-tours/pkg/passwords"

	"github.com/google/uuid"
)

type UserManager interface {
	GetUserById(id uuid.UUID) (*user.User, error)
	GetUserByEmail(email string) (*user.User, error)

	CreateUser(email, passwordHash, firstName, lastName string, role user.Role) (*user.User, error)
	CheckUserExists(email string) (bool, error)
}

type Service struct {
	userManager UserManager
}

func New(um UserManager) *Service {
	return &Service{
		userManager: um,
	}
}

func (s *Service) CreateUser(credentials *dto.RegistrationIn, role user.Role) (*user.User, error) {
	exists, err := s.userManager.CheckUserExists(credentials.Email)
	if err != nil {
		return nil, err
	}
	if exists {
		return nil, common.AlreadyExistsError{Object: "User"}
	}
	passwordHash, err := passwords.HashPassword(credentials.Password)
	if err != nil {
		return nil, err
	}
	return s.userManager.CreateUser(credentials.Email, passwordHash, credentials.FirstName, credentials.LastName, role)
}

func (s *Service) GetUserById(userId uuid.UUID) (*user.User, error) {
	return s.userManager.GetUserById(userId)
}

func (s *Service) GetUserByEmail(email string) (*user.User, error) {
	return s.userManager.GetUserByEmail(email)
}
