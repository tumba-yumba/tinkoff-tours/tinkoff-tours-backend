package api

import (
	"tinkoff-tours/pkg/jwt"
	"tinkoff-tours/pkg/s3"

	"tinkoff-tours/internal/auth"
	guideRepository "tinkoff-tours/internal/guide/db"
	guideService "tinkoff-tours/internal/guide/service"
	guideHandler "tinkoff-tours/internal/guide/transport"
	guideSerializer "tinkoff-tours/internal/guide/transport/serializers"
	photoRepository "tinkoff-tours/internal/photo/db"
	tourRepository "tinkoff-tours/internal/tour/db"
	tourService "tinkoff-tours/internal/tour/service"
	tourSerializers "tinkoff-tours/internal/tour/transport/serializers"
	userRepository "tinkoff-tours/internal/user/db"
	userService "tinkoff-tours/internal/user/service"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

func Configure(db *sqlx.DB, r *mux.Router, s *s3.S3, j *jwt.JWT) {
	ur := userRepository.New(db)

	gs := guideService.New(guideRepository.New(db), s)
	ts := tourService.New(tourRepository.New(db), photoRepository.New(db), s)
	us := userService.New(ur)

	m := auth.NewMiddlewareProvider(us, j)

	gser := guideSerializer.New(s)
	tser := tourSerializers.New(s)
	guideHandler.AddApiRoutes(r, guideHandler.New(gs, ts, gser, tser), m)
}
