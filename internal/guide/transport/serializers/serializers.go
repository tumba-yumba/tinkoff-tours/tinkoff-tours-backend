package serializers

import (
	"tinkoff-tours/internal/guide"
	"tinkoff-tours/internal/guide/transport/dto"
	"tinkoff-tours/pkg/s3"
)

type Serializer struct {
	s3 *s3.S3
}

func New(s *s3.S3) *Serializer {
	return &Serializer{s3: s}
}

func (s *Serializer) SerializeGuide(guide *guide.Guide) dto.GuideOut {
	return dto.GuideOut{
		Id:             guide.Id.String(),
		FirstName:      guide.FirstName,
		LastName:       guide.LastName,
		Bio:            guide.Bio,
		Avatar:         s.s3.GetFilePath(guide.Avatar),
		CreatedAt:      guide.CreatedAt.Format("2006-01-02"),
		ModerateStatus: string(guide.ModerateStatus),
	}
}

func (s *Serializer) SerializeSelfGuide(guide *guide.Guide) *dto.SelfGuideOut {
	return &dto.SelfGuideOut{
		Id:             guide.Id.String(),
		FirstName:      guide.FirstName,
		LastName:       guide.LastName,
		Bio:            guide.Bio,
		Email:          guide.Email,
		Avatar:         s.s3.GetFilePath(guide.Avatar),
		CreatedAt:      guide.CreatedAt.Format("2006-01-02"),
		ModerateStatus: string(guide.ModerateStatus),
	}
}

func (s *Serializer) SerializeGuidesList(guides []*guide.Guide, count int) dto.GuidesListOut {
	guidesList := make([]dto.GuideOut, 0)
	for _, guide := range guides {
		guidesList = append(guidesList, s.SerializeGuide(guide))
	}
	return dto.GuidesListOut{
		Count: count,
		Items: guidesList,
	}
}
