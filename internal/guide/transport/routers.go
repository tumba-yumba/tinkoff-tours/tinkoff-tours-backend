package transport

import (
	"tinkoff-tours/internal/auth"

	"github.com/gorilla/mux"
)

func AddApiRoutes(r *mux.Router, h *Handler, m auth.MiddlewareProvider) {
	g := r.PathPrefix("/guide").Subrouter()
	g.Use(m.AuthorizationMiddleware)
	g.HandleFunc("", h.GetGuide).Methods("GET")
	g.HandleFunc("", h.UpdateGuide).Methods("PUT")
	g.HandleFunc("/tours", h.GetGuideTours).Methods("GET")
	g.HandleFunc("/avatar", h.UploadAvatar).Methods("POST")
	g.HandleFunc("/avatar", h.DeleteAvatar).Methods("DELETE")

	gs := r.PathPrefix("/guides").Subrouter()
	gs.HandleFunc("/{id}", h.GetGuideById).Methods("GET")
	gs.HandleFunc("/{id}/tours", h.GetToursByGuideId).Methods("GET")
}
