package transport

import (
	"net/http"
	"tinkoff-tours/internal/common"
	guideService "tinkoff-tours/internal/guide/service"
	"tinkoff-tours/internal/guide/transport/dto"
	"tinkoff-tours/internal/guide/transport/serializers"
	tourService "tinkoff-tours/internal/tour/service"
	tourSerializers "tinkoff-tours/internal/tour/transport/serializers"
	"tinkoff-tours/pkg/responses"
	"tinkoff-tours/pkg/s3"
	"tinkoff-tours/pkg/utils"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

type Handler struct {
	guideService *guideService.Service
	tourService  *tourService.Service

	guideSerializer *serializers.Serializer
	tourSerializer  *tourSerializers.Serializer
}

func New(gs *guideService.Service, ts *tourService.Service, gser *serializers.Serializer, tser *tourSerializers.Serializer) *Handler {
	return &Handler{
		guideService:    gs,
		tourService:     ts,
		guideSerializer: gser,
		tourSerializer:  tser,
	}
}

// GetGuide
// @Summary Get tour
// @Description Get tour info
// @Tags guide
// @Produce json
// @Success 200 {object} dto.SelfGuideOut
// @Security BearerAuth
// @Router /guide [get]
func (h *Handler) GetGuide(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(r.Header.Get("X-UserId"))
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	guide, err := h.guideService.GetGuideById(guideId)
	if err != nil {
		switch err {
		case common.NotFoundError{Object: "Guide"}:
			responses.ResponseError(w, http.StatusNotFound, err)
		default:
			responses.ResponseError(w, http.StatusInternalServerError, err)
		}
		return
	}
	responses.ResponseData(w, http.StatusOK, h.guideSerializer.SerializeSelfGuide(guide))
}

// GetGuideById
// @Summary Get guide info
// @Description Get guide info
// @Tags guides
// @Produce json
// @Param id path string true "Guide ID"
// @Success 200 {object} dto.GuideOut
// @Router /guides/{id} [get]
func (h *Handler) GetGuideById(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	guide, err := h.guideService.GetGuideById(guideId)
	if err != nil {
		switch err {
		case common.NotFoundError{Object: "Guide"}:
			responses.ResponseError(w, http.StatusNotFound, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, http.StatusOK, h.guideSerializer.SerializeGuide(guide))
}

// UpdateGuide
// @Summary Update guide info
// @Description Update guide info
// @Tags guide
// @Accept json
// @Produce json
// @Param fields body dto.GuideIn false "Updated Fields"
// @Success 200 {object} dto.GuideOut
// @Security BearerAuth
// @Router /guide [put]
func (h *Handler) UpdateGuide(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(r.Header.Get("X-UserId"))
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	guideIn, err := utils.DecodeAndValidate[dto.GuideIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	guide, err := h.guideService.UpdateGuide(guideId, guideIn)
	if err != nil {
		switch err {
		case common.NotFoundError{Object: "Guide"}:
			responses.ResponseError(w, http.StatusNotFound, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, http.StatusOK, h.guideSerializer.SerializeGuide(guide))
}

// UploadAvatar
// @Summary Upload guide avatar
// @Description Upload guide avatar
// @Tags guide
// @Accept mpfd
// @Produce json
// @Param avatar formData file true "Photo File"
// @Success 200 {object} dto.AvatarOut
// @Security BearerAuth
// @Router /guide/avatar [post]
func (h *Handler) UploadAvatar(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(r.Header.Get("X-UserId"))
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	file, header, err := r.FormFile("avatar")
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "avatar"})
		return
	}
	id, filepath, err := h.guideService.UploadAvatar(guideId, header, file)
	if err != nil {
		switch err {
		case s3.PhotoExtError{}, s3.PhotoTooLargeError{}:
			responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, http.StatusOK, dto.AvatarOut{Id: id.String(), Path: filepath})
}

// DeleteAvatar
// @Summary Delete guide avatar
// @Description Delete guide avatar
// @Tags guide
// @Accept mpfd
// @Produce json
// @Success 200 {object} dto.AvatarOut
// @Security BearerAuth
// @Router /guide/avatar [delete]
func (h *Handler) DeleteAvatar(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(r.Header.Get("X-UserId"))
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	err = h.guideService.DeleteAvatar(guideId)
	if err != nil {
		switch err {
		case common.NotFoundError{Object: "Avatar"}:
			responses.ResponseError(w, http.StatusNotFound, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseSuccess(w)
}

// GetGuideTours
// @Summary Get guide tours
// @Description Get guide tourService pagination
// @Tags guide
// @Produce json
// @Param pagination query common.Pagination true "Pagination"
// @Success 200 {object} dto.ToursListOut
// @Security BearerAuth
// @Router /guide/tours [get]
func (h *Handler) GetGuideTours(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(r.Header.Get("X-UserId"))
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	pagination, err := utils.DecodeAndValidateQueryParam[common.Pagination](r.URL.Query())
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	toursCount, tours, err := h.tourService.GetToursByGuide(guideId, pagination)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseData(w, http.StatusOK, h.tourSerializer.SerializeToursList(tours, toursCount))
}

// GetToursByGuideId
// @Summary Get Tours by guide
// @Description Get tourService by guide with pagination
// @Tags guides
// @Produce json
// @Param id path string true "Guide ID"
// @Param pagination query common.Pagination true "Pagination"
// @Success 200 {object} dto.ToursListOut
// @Router /guides/{id}/tours [get]
func (h *Handler) GetToursByGuideId(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	pagination, err := utils.DecodeAndValidateQueryParam[common.Pagination](r.URL.Query())
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	toursCount, tours, err := h.tourService.GetToursByGuide(guideId, pagination)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseData(w, http.StatusOK, h.tourSerializer.SerializeToursList(tours, toursCount))
}
