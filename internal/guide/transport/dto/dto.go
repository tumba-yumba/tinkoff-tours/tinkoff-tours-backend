package dto

type GuideIn struct {
	FirstName *string `json:"first_name"`
	LastName  *string `json:"last_name"`
	Bio       *string `json:"bio"`
}

type SelfGuideOut struct {
	Id             string `json:"id"`
	FirstName      string `json:"first_name"`
	LastName       string `json:"last_name"`
	Email          string `json:"email"`
	Bio            string `json:"bio"`
	Avatar         string `json:"avatar"`
	CreatedAt      string `json:"created_at"`
	ModerateStatus string `json:"moderate_status"`
}

type GuideOut struct {
	Id             string `json:"id"`
	FirstName      string `json:"first_name"`
	LastName       string `json:"last_name"`
	Bio            string `json:"bio"`
	Avatar         string `json:"avatar"`
	CreatedAt      string `json:"created_at"`
	ModerateStatus string `json:"moderate_status"`
}

type GuidesListOut struct {
	Count int        `json:"count"`
	Items []GuideOut `json:"items"`
}

type AvatarOut struct {
	Id   string `json:"id"`
	Path string `json:"path"`
}
