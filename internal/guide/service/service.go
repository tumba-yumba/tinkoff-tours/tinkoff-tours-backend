package service

import (
	"github.com/google/uuid"
	"mime/multipart"
	"path/filepath"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/guide"
	"tinkoff-tours/internal/guide/transport/dto"
)

type GuideManager interface {
	GetGuideById(id uuid.UUID) (*guide.Guide, error)

	UpdateGuide(id uuid.UUID, data dto.GuideIn) (*guide.Guide, error)
	UploadAvatar(id uuid.UUID, ext string) (uuid.UUID, string, error)
	DeleteAvatar(id uuid.UUID) error
}

type S3Manager interface {
	ValidatePhoto(fileHeader *multipart.FileHeader) error
	UploadFile(filename string, file multipart.File) (string, error)
}

type Service struct {
	guideManager GuideManager
	s3Manager    S3Manager
}

func New(gm GuideManager, s S3Manager) *Service {
	return &Service{
		guideManager: gm,
		s3Manager:    s,
	}
}

func (s *Service) GetGuideById(id uuid.UUID) (*guide.Guide, error) {
	return s.guideManager.GetGuideById(id)
}

func (s *Service) UpdateGuide(id uuid.UUID, data dto.GuideIn) (*guide.Guide, error) {
	return s.guideManager.UpdateGuide(id, data)
}

func (s *Service) UploadAvatar(guideId uuid.UUID, fileHeader *multipart.FileHeader, file multipart.File) (uuid.UUID, string, error) {
	if err := s.s3Manager.ValidatePhoto(fileHeader); err != nil {
		return uuid.Nil, "", err
	}
	ext := filepath.Ext(fileHeader.Filename)
	id, filename, err := s.guideManager.UploadAvatar(guideId, ext)
	if err != nil {
		return uuid.Nil, "", err
	}
	filepath, err := s.s3Manager.UploadFile(filename, file)
	return id, filepath, err
}

func (s *Service) DeleteAvatar(guideId uuid.UUID) error { // TODO: delete from s3
	guide, err := s.guideManager.GetGuideById(guideId)
	if err != nil {
		return err
	}
	if guide.Avatar == "" {
		return common.NotFoundError{Object: "Avatar"}
	}
	return s.guideManager.DeleteAvatar(guideId)
}
