-- name: update-guide
UPDATE
    users
SET first_name      = coalesce(:first_name, first_name),
    last_name       = coalesce(:last_name, last_name),
    bio             = coalesce(:bio, bio),
    moderate_status = 'pending'
WHERE id = :id
RETURNING
    *;

-- name: update-guide-avatar
UPDATE
    users
SET avatar          = $1,
    moderate_status = 'pending'
WHERE id = $2;

-- name: delete-guide-avatar
UPDATE
    users
SET avatar          = '',
    moderate_status = 'pending'
WHERE id = $1;

-- name: get-unmoderated-guides-list
SELECT *
FROM users
WHERE moderate_status = 'pending'
  AND ROLE = 'guide'
LIMIT $1 OFFSET $2;

-- name: get-unmoderated-guides-count
SELECT count(*)
FROM users
WHERE moderate_status = 'pending'
  AND ROLE = 'guide';

