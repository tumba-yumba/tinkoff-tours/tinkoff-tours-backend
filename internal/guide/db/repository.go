package db

import (
	"database/sql"
	"fmt"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/guide"
	"tinkoff-tours/internal/guide/transport/dto"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/nleof/goyesql"
)

type Repository struct {
	*sqlx.DB
	queries goyesql.Queries
}

func New(db *sqlx.DB) *Repository {
	return &Repository{
		db,
		goyesql.MustParseFile("internal/guide/db/queries.sql"),
	}
}

func (r *Repository) UpdateGuide(id uuid.UUID, data dto.GuideIn) (*guide.Guide, error) {
	nstmt, err := r.PrepareNamed(r.queries["update-guide"])
	if err != nil {
		return nil, fmt.Errorf("error while prepare named stmt in update guide: %w", err)
	}
	dataIn := map[string]interface{}{
		"id":         id,
		"first_name": data.FirstName,
		"last_name":  data.LastName,
		"bio":        data.Bio,
	}
	var guide guide.Guide
	if err := nstmt.Get(&guide, dataIn); err != nil {
		if err == sql.ErrNoRows {
			return nil, common.NotFoundError{Object: "Guide"}
		}
		return nil, fmt.Errorf("error while update guide: %w", err)
	}
	return &guide, nil
}

func (r *Repository) UploadAvatar(guideId uuid.UUID, photoExt string) (id uuid.UUID, filename string, err error) {
	id, err = uuid.NewUUID()
	if err != nil {
		return uuid.Nil, "", fmt.Errorf("error while generate uuid for avatar: %w", err)
	}
	filename = id.String() + photoExt
	if _, err := r.Exec(r.queries["update-guide-avatar"], filename, guideId); err != nil {
		return uuid.Nil, "", fmt.Errorf("error while update guide avatar: %w", err)
	}
	return id, filename, nil
}

func (r *Repository) DeleteAvatar(guideId uuid.UUID) error {
	if _, err := r.Exec(r.queries["delete-guide-avatar"], guideId); err != nil {
		return fmt.Errorf("error while delete avatar: %w", err)
	}
	return nil
}

func (r *Repository) ChangeModerationStatus(guideId uuid.UUID, status string) error {
	_, err := r.Exec("UPDATE users SET moderate_status = $1 WHERE id = $2", status, guideId)
	if err != nil {
		return fmt.Errorf("error while change guide moderation status: %w", err)
	}
	return nil
}

func (r *Repository) GetUnmoderatedGuides(pagination common.Pagination) ([]*guide.Guide, error) {
	var guides []*guide.Guide
	if err := r.Select(&guides, r.queries["get-unmoderated-guides-list"], pagination.Limit, pagination.Offset); err != nil {
		return nil, fmt.Errorf("error while get unmoderated guides: %w", err)
	}
	return guides, nil
}

func (r *Repository) GetUnmoderatedGuidesCount() (int, error) {
	var count int
	if err := r.Get(&count, r.queries["get-unmoderated-guides-count"]); err != nil {
		return 0, fmt.Errorf("error while get unmoderated guides count: %w", err)
	}
	return count, nil
}

func (r *Repository) GetGuideById(id uuid.UUID) (*guide.Guide, error) {
	var guide guide.Guide
	err := r.QueryRowx("SELECT * FROM users WHERE id = $1", id).StructScan(&guide)
	if err != nil {
		if err != sql.ErrNoRows {
			return nil, fmt.Errorf("error while get guide by id: %w", err)
		}
		return nil, common.NotFoundError{Object: "Guide"}
	}
	return &guide, nil
}
