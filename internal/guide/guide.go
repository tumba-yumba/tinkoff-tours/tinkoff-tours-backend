package guide

import (
	"tinkoff-tours/internal/moderation"
	"tinkoff-tours/internal/user"
)

type Guide struct {
	*user.User
	Avatar         string            `db:"avatar"`
	Bio            string            `db:"bio"`
	ModerateStatus moderation.Status `db:"moderate_status"`
}
