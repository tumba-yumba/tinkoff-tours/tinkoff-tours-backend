package internal

import (
	"tinkoff-tours/configs"
	authApi "tinkoff-tours/internal/auth/api"
	bookingApi "tinkoff-tours/internal/booking/api"
	feedbackApi "tinkoff-tours/internal/feedback/api"
	guideApi "tinkoff-tours/internal/guide/api"
	moderationApi "tinkoff-tours/internal/moderation/api"
	tagApi "tinkoff-tours/internal/tag/api"
	tourApi "tinkoff-tours/internal/tour/api"
	"tinkoff-tours/pkg/db"
	"tinkoff-tours/pkg/jwt"

	"tinkoff-tours/pkg/email"
	"tinkoff-tours/pkg/s3"

	httpSwagger "github.com/swaggo/http-swagger"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

type App struct {
	db     *sqlx.DB
	s3     *s3.S3
	email  *email.Sender
	jwt    *jwt.JWT
	config *configs.Config
	router *mux.Router
}

func New(cfg *configs.Config) *App {
	return &App{config: cfg}
}

func (a *App) Init() (*App, error) {
	var err error
	a.db, err = db.New(a.config)
	if err != nil {
		return a, err
	}
	a.s3, err = s3.New(a.config)
	if err != nil {
		return a, err
	}
	a.email = email.New(a.config)
	a.jwt = jwt.New(a.config.JWTSecret)
	a.router = mux.NewRouter().PathPrefix("/api").Subrouter()
	return a, nil
}

func (a *App) GetApiRouters() *mux.Router {
	a.router.PathPrefix("/docs").Handler(httpSwagger.WrapHandler)

	bookingApi.Configure(a.db, a.router, a.email, a.jwt, a.config.Url, a.config.UnbookingUrl)
	tagApi.Configure(a.db, a.router)
	moderationApi.Configure(a.db, a.router, a.s3, a.jwt)
	guideApi.Configure(a.db, a.router, a.s3, a.jwt)
	authApi.Configure(a.db, a.router, a.jwt, a.config.AccessTokenExpiresInMinute, a.config.RefreshTokenExpiresInMinute)
	tourApi.Configure(a.db, a.router, a.s3, a.jwt)
	feedbackApi.Configure(a.db, a.router, a.s3, a.jwt)

	return a.router
}
