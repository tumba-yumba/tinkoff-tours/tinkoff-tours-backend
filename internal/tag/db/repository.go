package db

import (
	"fmt"
	"tinkoff-tours/internal/tag"

	"github.com/jmoiron/sqlx"
	"github.com/nleof/goyesql"
)

type Repository struct {
	*sqlx.DB
	queries goyesql.Queries
}

func New(db *sqlx.DB) *Repository {
	return &Repository{
		db,
		goyesql.MustParseFile("internal/tag/db/queries.sql"),
	}
}

func (r *Repository) GetTags() ([]tag.Tag, error) {
	var tags []tag.Tag
	if err := r.Select(&tags, r.queries["get-tags-list"]); err != nil {
		return nil, fmt.Errorf("error while get tags: %w", err)
	}
	return tags, nil
}
