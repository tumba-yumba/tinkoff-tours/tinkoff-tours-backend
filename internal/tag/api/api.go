package api

import (
	tagRepository "tinkoff-tours/internal/tag/db"
	"tinkoff-tours/internal/tag/service"
	tagHandler "tinkoff-tours/internal/tag/transport"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

func Configure(d *sqlx.DB, r *mux.Router) {
	ts := service.New(
		tagRepository.New(d),
	)
	tagHandler.AddApiRoutes(r, tagHandler.New(ts))
}
