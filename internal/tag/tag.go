package tag

type Tag struct {
	Id    int    `db:"id"`
	Title string `db:"title"`
}
