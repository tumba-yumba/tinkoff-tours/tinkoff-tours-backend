package dto

type TagsOut struct {
	Tags []TagOut `json:"tags"`
}

type TagOut struct {
	Id    string `json:"id"`
	Title string `json:"title"`
}
