package transport

import "github.com/gorilla/mux"

func AddApiRoutes(r *mux.Router, h *Handler) {
	t := r.PathPrefix("/tags").Subrouter()
	t.HandleFunc("", h.GetTags).Methods("GET")
}
