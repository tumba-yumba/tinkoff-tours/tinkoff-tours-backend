package transport

import (
	"net/http"
	"strconv"
	"tinkoff-tours/internal/tag"
	tagService "tinkoff-tours/internal/tag/service"
	"tinkoff-tours/internal/tag/transport/dto"
	"tinkoff-tours/pkg/responses"
)

type Handler struct {
	tagService *tagService.Service
}

func New(ts *tagService.Service) *Handler {
	return &Handler{tagService: ts}
}

// @Summary Get tags
// @Description Get tags
// @Tags tags
// @Accept json
// @Produce json
// @Success 200 {object} dto.TagsOut
// @Router /tags [get]
func (h *Handler) GetTags(w http.ResponseWriter, r *http.Request) {
	tagsList, err := h.tagService.GetTags()
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseData(w, http.StatusOK, SerializeTags(tagsList))
}

func SerializeTags(tags []tag.Tag) dto.TagsOut {
	result := make([]dto.TagOut, 0)
	for _, tag := range tags {
		result = append(result, dto.TagOut{Id: strconv.Itoa(tag.Id), Title: tag.Title})
	}
	return dto.TagsOut{Tags: result}
}
