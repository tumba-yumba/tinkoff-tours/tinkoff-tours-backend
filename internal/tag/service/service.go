package service

import (
	"tinkoff-tours/internal/tag"
)

type TagsProvider interface {
	GetTags() ([]tag.Tag, error)
}

type Service struct {
	tagsProvider TagsProvider
}

func New(tp TagsProvider) *Service {
	return &Service{
		tp,
	}
}

func (ts *Service) GetTags() ([]tag.Tag, error) {
	return ts.tagsProvider.GetTags()
}
