package db

import (
	"fmt"
	"tinkoff-tours/internal/photo"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/nleof/goyesql"
)

type Repository struct {
	*sqlx.DB
	queries goyesql.Queries
}

func New(db *sqlx.DB) *Repository {
	return &Repository{
		db,
		goyesql.MustParseFile("internal/photo/db/queries.sql"),
	}
}

func (r *Repository) CreatePhoto(photoExt string) (*photo.Photo, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return nil, fmt.Errorf("error while create tour uuid: %w", err)
	}
	var p photo.Photo
	if err := r.Get(&p, r.queries["create-photo"], id, id.String()+photoExt); err != nil {
		return nil, fmt.Errorf("error while create photo: %w", err)
	}
	return &p, nil
}

func (r *Repository) DeletePhoto(id string) error {
	if _, err := r.Exec(r.queries["delete-photo"], id); err != nil { //TODO: fix
		return fmt.Errorf("error while delete photo: %w", err)
	}
	return nil
}
