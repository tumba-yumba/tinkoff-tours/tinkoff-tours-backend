-- name: create-photo
INSERT INTO photos (id, filename)
VALUES ($1, $2)
RETURNING
    *;

-- name: delete-photo
DELETE
FROM photos
WHERE id = $1;

