package photo

import "github.com/google/uuid"

type Photo struct {
	Id       uuid.UUID `db:"id"`
	TourId   uuid.UUID `db:"tour_id"`
	Filename string    `db:"filename"`
}
