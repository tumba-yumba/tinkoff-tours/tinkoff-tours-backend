package dto

import "github.com/google/uuid"

type FeedbackIn struct {
	Comment     string            `json:"comment" validation:"required"`
	GuideRating int               `json:"guide_rating" validation:"required,gte=0,lte=5"`
	TourRating  int               `json:"tour_rating" validation:"required,gte=0,lte=5"`
	Photos      []FeedbackPhotoIn `json:"photos"`
	Estimations EstimationsIn     `json:"estimations"`
}

type EstimationsIn struct {
	Boring         bool `json:"boring"`
	KnowNothing    bool `json:"know_nothing"`
	NotHeard       bool `json:"not_heard"`
	Interesting    bool `json:"interesting"`
	KnowEverything bool `json:"know_everything"`
	Entertainment  bool `json:"entertainment"`

	NotImpressed  bool `json:"not_impressed"`
	BadWeather    bool `json:"bad_weather"`
	Uncomfortable bool `json:"uncomfortable"`
	Picturesque   bool `json:"picturesque"`
	Comfortable   bool `json:"comfortable"`
	Adrenaline    bool `json:"adrenaline"`
}

type FeedbackPhotoIn struct {
	Id string `json:"id" validation:"required,uuid"`
}

type FeedbackPhotoOut struct {
	Id   string `json:"id" validation:"required,uuid"`
	Path string `json:"path" validation:"required"`
}

type GuideEstimationsOut struct {
	Boring         EstimationsCountOut `json:"boring"`
	KnowNothing    EstimationsCountOut `json:"know_nothing"`
	NotHeard       EstimationsCountOut `json:"not_heard"`
	Interesting    EstimationsCountOut `json:"interesting"`
	KnowEverything EstimationsCountOut `json:"know_everything"`
	Entertainment  EstimationsCountOut `json:"entertainment"`
	GuideRating    float32             `json:"guide_rating"`
}

type TourEstimationsOut struct {
	NotImpressed  EstimationsCountOut `json:"not_impressed"`
	BadWeather    EstimationsCountOut `json:"bad_weather"`
	Uncomfortable EstimationsCountOut `json:"uncomfortable"`
	Picturesque   EstimationsCountOut `json:"picturesque"`
	Comfortable   EstimationsCountOut `json:"comfortable"`
	Adrenaline    EstimationsCountOut `json:"adrenaline"`
	TourRating    float32             `json:"tour_rating"`
}

type EstimationsCountOut struct {
	Count int `json:"count"`
}

type FeedbackListOut struct {
	Items []FeedbackInList `json:"items"`
}

type FeedbackInList struct {
	Id            uuid.UUID `json:"id"`
	Name          string    `json:"name"`
	Comment       string    `json:"comment"`
	TourRating    int       `json:"tour_rating"`
	CreatedAt     string    `json:"created_at"`
	Photos        []string  `json:"photos"`
	NotImpressed  bool      `json:"not_impressed"`
	BadWeather    bool      `json:"bad_weather"`
	Uncomfortable bool      `json:"uncomfortable"`
	Picturesque   bool      `json:"picturesque"`
	Comfortable   bool      `json:"comfortable"`
	Adrenaline    bool      `json:"adrenaline"`
}
