package transport

import (
	"net/http"
	"tinkoff-tours/internal/auth"

	"github.com/gorilla/mux"
)

func AddApiRoutes(r *mux.Router, h *Handler, m auth.MiddlewareProvider) {
	r.HandleFunc("/tours/{id}/feedbacks", h.CreateFeedback).Methods("POST")
	r.HandleFunc("/tours/{id}/feedbacks", h.GetTourFeedbacks).Methods("GET")
	r.Handle("/moderation/feedbacks", m.AuthorizationMiddleware(http.HandlerFunc(h.GetAllFeedbacks))).Methods("GET")

	feedbackRouter := r.PathPrefix("/feedbacks").Subrouter()
	feedbackRouter.HandleFunc("/photos", h.UploadPhoto).Methods("POST")
	feedbackRouter.HandleFunc("/photos", h.DeletePhoto).Methods("DELETE")

	r.HandleFunc("/guides/{id}/estimations", h.GetGuideEstimations).Methods("GET")
	r.HandleFunc("/tours/{id}/estimations", h.GetTourEstimations).Methods("GET")
}
