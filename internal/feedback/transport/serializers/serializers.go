package serializers

import (
	"tinkoff-tours/internal/feedback"
	"tinkoff-tours/internal/feedback/transport/dto"
	"tinkoff-tours/pkg/s3"
)

type Serializer struct {
	s3 *s3.S3
}

func New(s3 *s3.S3) *Serializer {
	return &Serializer{s3: s3}
}

func (s *Serializer) SerializeGuideEstimationsCount(est *feedback.GuideEstimations) *dto.GuideEstimationsOut {
	return &dto.GuideEstimationsOut{
		Boring:         dto.EstimationsCountOut{Count: est.BoringCount},
		KnowNothing:    dto.EstimationsCountOut{Count: est.KnowNothingCount},
		NotHeard:       dto.EstimationsCountOut{Count: est.NotHeardCount},
		Interesting:    dto.EstimationsCountOut{Count: est.InterestingCount},
		KnowEverything: dto.EstimationsCountOut{Count: est.KnowEverythingCount},
		Entertainment:  dto.EstimationsCountOut{Count: est.EntertainmentCount},
		GuideRating:    est.Rating,
	}
}

func (s *Serializer) SerializeTourEstimationsCount(est *feedback.TourEstimations) *dto.TourEstimationsOut {
	return &dto.TourEstimationsOut{
		NotImpressed:  dto.EstimationsCountOut{Count: est.NotImpressedCount},
		BadWeather:    dto.EstimationsCountOut{Count: est.BadWeatherCount},
		Uncomfortable: dto.EstimationsCountOut{Count: est.UncomfortableCount},
		Picturesque:   dto.EstimationsCountOut{Count: est.PicturesqueCount},
		Comfortable:   dto.EstimationsCountOut{Count: est.ComfortableCount},
		Adrenaline:    dto.EstimationsCountOut{Count: est.AdrenalineCount},
		TourRating:    est.Rating,
	}
}

func (s *Serializer) SerializeFeedbackList(feedbacks []feedback.FeedbackInList) dto.FeedbackListOut {
	feedbacksOut := make([]dto.FeedbackInList, 0)
	for _, fb := range feedbacks {
		feedbacksOut = append(feedbacksOut, s.SerializeFeedbackInList(fb))
	}
	return dto.FeedbackListOut{
		Items: feedbacksOut,
	}
}

func (s *Serializer) SerializeFeedbackInList(fb feedback.FeedbackInList) dto.FeedbackInList {
	return dto.FeedbackInList{
		Id:            fb.Id,
		Name:          fb.Name,
		Comment:       fb.Comment,
		TourRating:    fb.TourRating,
		CreatedAt:     fb.CreatedAt.Format("2006-01-02 15:04"),
		Photos:        s.SerializePhotos(fb.Photos),
		NotImpressed:  fb.NotImpressed,
		BadWeather:    fb.BadWeather,
		Uncomfortable: fb.Uncomfortable,
		Picturesque:   fb.Picturesque,
		Comfortable:   fb.Comfortable,
		Adrenaline:    fb.Adrenaline,
	}
}

func (s *Serializer) SerializePhotos(photos []string) []string {
	res := make([]string, 0)
	for _, photo := range photos {
		res = append(res, s.s3.GetFilePath(photo))
	}
	return res
}
