package transport

import (
	"errors"
	"net/http"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/feedback/service"
	"tinkoff-tours/internal/feedback/transport/dto"
	feedbackSerializers "tinkoff-tours/internal/feedback/transport/serializers"
	"tinkoff-tours/pkg/responses"
	"tinkoff-tours/pkg/s3"
	"tinkoff-tours/pkg/utils"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

type Handler struct {
	feedbackService *service.Service

	feedbackSerializer *feedbackSerializers.Serializer
}

func New(fs *service.Service, fSerializer *feedbackSerializers.Serializer) *Handler {
	return &Handler{
		feedbackService:    fs,
		feedbackSerializer: fSerializer,
	}
}

// CreateFeedback
// @Summary Create feedback
// @Description Create feedback
// @Tags feedbacks
// @Produce json
// @Param id path string true "Tour ID"
// @Param booking_id query string true "Booking ID"
// @Param feedbackData body dto.FeedbackIn true "Feedback Data"
// @Success 200 {object} responses.SuccessOK
// @Router /tours/{id}/feedbacks [post]
func (h *Handler) CreateFeedback(w http.ResponseWriter, r *http.Request) {
	tourId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	bookingId, err := uuid.Parse(r.URL.Query().Get("booking_id"))
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "booking id"})
		return
	}
	data, err := utils.DecodeAndValidate[dto.FeedbackIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	if err = h.feedbackService.CreateFeedback(bookingId, tourId, data); err != nil {
		switch err {
		case common.AlreadyExistsError{Object: "Feedback"}:
			responses.ResponseError(w, 409, err)
		case common.NotFoundError{Object: "Booking ID"}:
			responses.ResponseError(w, 404, errors.New("not able to create feedback"))
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseSuccess(w)
}

// UploadPhoto
// @Summary Upload photo to feedback
// @Description Upload photo to feedback
// @Tags feedbacks
// @Accept mpfd
// @Produce json
// @Param photo formData file true "Photo File"
// @Success 200 {object} dto.FeedbackPhotoOut
// @Router /feedbacks/photos [post]
func (h *Handler) UploadPhoto(w http.ResponseWriter, r *http.Request) {
	file, header, err := r.FormFile("photo")
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "photo"})
		return
	}
	id, path, err := h.feedbackService.UploadPhoto(header, file)
	if err != nil {
		switch err {
		case s3.PhotoExtError{}, s3.PhotoTooLargeError{}:
			responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, http.StatusOK, &dto.FeedbackPhotoOut{Id: id.String(), Path: path})
}

// DeletePhoto
// @Summary Delete photo in feedback
// @Description delete photo in feedback
// @Tags feedbacks
// @Accept json
// @Produce json
// @Param photoData body dto.FeedbackPhotoIn true "Photo Data"
// @Success 200 {object} responses.SuccessOK
// @Router /feedbacks/photos [delete]
func (h *Handler) DeletePhoto(w http.ResponseWriter, r *http.Request) {
	data, err := utils.DecodeAndValidate[dto.FeedbackPhotoIn](r.Body)
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, err)
		return
	}
	if err := h.feedbackService.DeletePhoto(data.Id); err != nil {
		switch err {
		case common.NotFoundError{Object: "Tour"}:
			responses.ResponseError(w, http.StatusNotFound, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseSuccess(w)
}

// GetGuideEstimations
// @Summary Get guide estimations
// @Description Get guide estimations
// @Tags guides
// @Produce json
// @Param id path string true "Guide ID"
// @Success 200 {object} dto.GuideEstimationsOut
// @Router /guides/{id}/estimations [get]
func (h *Handler) GetGuideEstimations(w http.ResponseWriter, r *http.Request) {
	guideId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	estimationsCount, err := h.feedbackService.GetGuideEstimations(guideId)
	if err != nil {
		switch err {
		case common.NotFoundError{Object: "Feedbacks"}:
			responses.ResponseError(w, 404, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, 200, h.feedbackSerializer.SerializeGuideEstimationsCount(estimationsCount))
}

// GetTourEstimations
// @Summary Get tour estimations
// @Description Get tour estimations
// @Tags tours
// @Produce json
// @Param id path string true "Tour ID"
// @Success 200 {object} dto.TourEstimationsOut
// @Router /tours/{id}/estimations [get]
func (h *Handler) GetTourEstimations(w http.ResponseWriter, r *http.Request) {
	tourId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	estimationsCount, err := h.feedbackService.GetTourEstimations(tourId)
	if err != nil {
		switch err {
		case common.NotFoundError{Object: "Feedbacks"}:
			responses.ResponseError(w, 404, err)
		default:
			responses.ResponseInternalError(w, err)
		}
		return
	}
	responses.ResponseData(w, 200, h.feedbackSerializer.SerializeTourEstimationsCount(estimationsCount))
}

// GetTourFeedbacks
// @Summary Get tour feedbacks
// @Description Get tour feedbacks
// @Tags tours
// @Produce json
// @Param id path string true "Tour ID"
// @Success 200 {object} dto.FeedbackListOut
// @Router /tours/{id}/feedbacks [get]
func (h *Handler) GetTourFeedbacks(w http.ResponseWriter, r *http.Request) {
	tourId, err := uuid.Parse(mux.Vars(r)["id"])
	if err != nil {
		responses.ResponseError(w, http.StatusUnprocessableEntity, common.ValidationError{Field: "id"})
		return
	}
	feedbacks, err := h.feedbackService.GetTourFeedbacks(tourId)
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseData(w, 200, h.feedbackSerializer.SerializeFeedbackList(feedbacks))
}

// GetTourFeedbacks
// @Summary Get all feedbacks
// @Description Get all feedbacks
// @Tags moderation
// @Produce json
// @Success 200 {object} dto.FeedbackListOut
// @Router /moderation/feedbacks [get]
func (h *Handler) GetAllFeedbacks(w http.ResponseWriter, r *http.Request) {
	feedbacks, err := h.feedbackService.GetAllFeedbacks()
	if err != nil {
		responses.ResponseInternalError(w, err)
		return
	}
	responses.ResponseData(w, 200, h.feedbackSerializer.SerializeFeedbackList(feedbacks))
}
