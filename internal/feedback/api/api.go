package api

import (
	"tinkoff-tours/internal/auth"
	feedbackRepository "tinkoff-tours/internal/feedback/db"
	feedbackService "tinkoff-tours/internal/feedback/service"
	feedbackHandler "tinkoff-tours/internal/feedback/transport"
	feedbackSerializers "tinkoff-tours/internal/feedback/transport/serializers"
	userRepository "tinkoff-tours/internal/user/db"
	userService "tinkoff-tours/internal/user/service"
	"tinkoff-tours/pkg/jwt"
	"tinkoff-tours/pkg/s3"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

func Configure(db *sqlx.DB, r *mux.Router, s *s3.S3, j *jwt.JWT) {
	fs := feedbackService.New(
		feedbackRepository.New(db),
		s,
	)
	ur := userRepository.New(db)
	us := userService.New(ur)

	m := auth.NewMiddlewareProvider(us, j)

	feedbackHandler.AddApiRoutes(r, feedbackHandler.New(fs, feedbackSerializers.New(s)), m)
}
