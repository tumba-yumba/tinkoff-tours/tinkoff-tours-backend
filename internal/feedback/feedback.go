package feedback

import (
	"time"

	"github.com/google/uuid"
)

type Feedback struct {
	Id        uuid.UUID `db:"id"`
	TourId    uuid.UUID `db:"tour_id"`
	BookingId uuid.UUID `db:"booking_id"`
	Email     string    `db:"email"`
	Comment   string    `db:"comment"`

	GuideRating int `db:"guide_rating"`
	TourRating  int `db:"tour_rating"`

	Photos      []Photo  `db:"photos"`
	Estimations []string `db:"estimations"`
}

type FeedbackInList struct {
	Id         uuid.UUID `db:"id"`
	Name       string    `db:"name"`
	Comment    string    `db:"comment"`
	TourRating int       `db:"tour_rating"`
	CreatedAt  time.Time `db:"created_at"`
	Photos     []string  `db:"photos"`

	NotImpressed  bool `db:"not_impressed"`
	BadWeather    bool `db:"bad_weather"`
	Uncomfortable bool `db:"uncomfortable"`
	Picturesque   bool `db:"picturesque"`
	Comfortable   bool `db:"comfortable"`
	Adrenaline    bool `db:"adrenaline"`
}

type Photo struct {
	Id         uuid.UUID `db:"id"`
	FeedbackId uuid.UUID `db:"feedback_id"`
	Filename   string    `db:"filename"`
}

type GuideEstimations struct {
	BoringCount         int `db:"boring_count"`
	KnowNothingCount    int `db:"know_nothing_count"`
	NotHeardCount       int `db:"not_heard_count"`
	InterestingCount    int `db:"interesting_count"`
	KnowEverythingCount int `db:"know_everything_count"`
	EntertainmentCount  int `db:"entertainment_count"`

	Rating float32 `db:"rating"`
}

type TourEstimations struct {
	NotImpressedCount  int `db:"not_impressed_count"`
	BadWeatherCount    int `db:"bad_weather_count"`
	UncomfortableCount int `db:"uncomfortable_count"`
	PicturesqueCount   int `db:"picturesque_count"`
	ComfortableCount   int `db:"comfortable_count"`
	AdrenalineCount    int `db:"adrenaline_count"`

	Rating float32 `db:"rating"`
}
