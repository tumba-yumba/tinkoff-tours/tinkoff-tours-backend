package service

import (
	"mime/multipart"
	"path/filepath"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/feedback"
	"tinkoff-tours/internal/feedback/transport/dto"

	"github.com/google/uuid"
)

type FeedbackManager interface {
	CreateFeedback(bookingId, tourId uuid.UUID, data dto.FeedbackIn) error

	CreatePhoto(ext string) (*feedback.Photo, error)
	DeletePhoto(id string) error

	GetGuideEstimations(guideId uuid.UUID) (*feedback.GuideEstimations, error)
	GetTourEstimations(tourId uuid.UUID) (*feedback.TourEstimations, error)
	GetTourFeedbacks(tourId uuid.UUID) ([]feedback.FeedbackInList, error)
	GetAllFeedbacks() ([]feedback.FeedbackInList, error)

	CheckFeedbackExistsByBookingId(bookingId uuid.UUID) (bool, error)
	CheckBookingExists(bookingId uuid.UUID) (bool, error)
}

type S3Manager interface {
	ValidatePhoto(header *multipart.FileHeader) error
	UploadFile(filename string, file multipart.File) (string, error)
}

type Service struct {
	feedbackManager FeedbackManager
	s3Manager       S3Manager
}

func New(fm FeedbackManager, s3 S3Manager) *Service {
	return &Service{
		feedbackManager: fm,
		s3Manager:       s3,
	}
}

func (s *Service) CreateFeedback(bookingId, tourId uuid.UUID, data dto.FeedbackIn) error {
	exists, err := s.feedbackManager.CheckFeedbackExistsByBookingId(bookingId)
	if err != nil {
		return err
	}
	if exists {
		return common.AlreadyExistsError{Object: "Feedback"}
	}
	exists, err = s.feedbackManager.CheckBookingExists(bookingId)
	if err != nil {
		return err
	}
	if !exists {
		return common.NotFoundError{Object: "Booking ID"}
	}
	return s.feedbackManager.CreateFeedback(bookingId, tourId, data)
}

func (s *Service) UploadPhoto(fileHeader *multipart.FileHeader, file multipart.File) (uuid.UUID, string, error) {
	if err := s.s3Manager.ValidatePhoto(fileHeader); err != nil {
		return uuid.Nil, "", err
	}
	ext := filepath.Ext(fileHeader.Filename)
	p, err := s.feedbackManager.CreatePhoto(ext)
	if err != nil {
		return uuid.Nil, "", err
	}
	path, err := s.s3Manager.UploadFile(p.Filename, file)
	return p.Id, path, err
}

func (s *Service) DeletePhoto(id string) error {
	return s.feedbackManager.DeletePhoto(id)
}

func (s *Service) GetGuideEstimations(guideId uuid.UUID) (*feedback.GuideEstimations, error) {
	return s.feedbackManager.GetGuideEstimations(guideId)
}

func (s *Service) GetTourEstimations(tourId uuid.UUID) (*feedback.TourEstimations, error) {
	return s.feedbackManager.GetTourEstimations(tourId)
}

func (s *Service) GetTourFeedbacks(tourId uuid.UUID) ([]feedback.FeedbackInList, error) {
	return s.feedbackManager.GetTourFeedbacks(tourId)
}

func (s *Service) GetAllFeedbacks() ([]feedback.FeedbackInList, error) {
	return s.feedbackManager.GetAllFeedbacks()
}
