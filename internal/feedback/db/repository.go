package db

import (
	"database/sql"
	"fmt"
	"strings"
	"time"
	"tinkoff-tours/internal/common"
	"tinkoff-tours/internal/feedback"
	"tinkoff-tours/internal/feedback/transport/dto"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/nleof/goyesql"
)

type Repository struct {
	*sqlx.DB
	queries goyesql.Queries
}

func New(d *sqlx.DB) *Repository {
	return &Repository{
		DB:      d,
		queries: goyesql.MustParseFile("internal/feedback/db/queries.sql"),
	}
}

func (r *Repository) CreateFeedback(bookingId, tourId uuid.UUID, data dto.FeedbackIn) error {
	tx, err := r.Beginx()
	defer tx.Rollback()
	if err != nil {
		return fmt.Errorf("error while begin: %w", err)
	}

	dataIn := map[string]interface{}{
		"tour_id":      tourId,
		"booking_id":   bookingId,
		"comment":      data.Comment,
		"guide_rating": data.GuideRating,
		"tour_rating":  data.TourRating,

		"boring":          data.Estimations.Boring,
		"know_nothing":    data.Estimations.KnowNothing,
		"not_heard":       data.Estimations.NotHeard,
		"interesting":     data.Estimations.Interesting,
		"know_everything": data.Estimations.KnowEverything,
		"entertainment":   data.Estimations.Entertainment,

		"not_impressed": data.Estimations.NotImpressed,
		"bad_weather":   data.Estimations.BadWeather,
		"uncomfortable": data.Estimations.Uncomfortable,
		"picturesque":   data.Estimations.Picturesque,
		"comfortable":   data.Estimations.Comfortable,
		"adrenaline":    data.Estimations.Adrenaline,
	}
	nstmt, err := tx.PrepareNamed(r.queries["create-feedback"])
	if err != nil {
		return fmt.Errorf("error while prepare create feedback: %w", err)
	}
	var id uuid.UUID
	if err = nstmt.Get(&id, dataIn); err != nil {
		return fmt.Errorf("error while create feedback: %w", err)
	}

	photoIds := make([]string, 0)
	for _, photo := range data.Photos {
		photoIds = append(photoIds, photo.Id)
	}
	if len(photoIds) > 0 {
		updatePhotosData := map[string]interface{}{"feedback_id": id, "photo_ids": photoIds}
		query, args, err := r.prepareQueryWithNamedAndINParameters(r.queries["update-feedback-photo"], updatePhotosData)
		if err != nil {
			return fmt.Errorf("error while prepare query: %w", err)
		}
		if _, err := tx.Exec(query, args...); err != nil {
			return fmt.Errorf("error while updated photos in feedback: %w", err)
		}
	}
	if err := tx.Commit(); err != nil {
		return fmt.Errorf("error while commit: %w", err)
	}
	return err
}

func (r *Repository) prepareQueryWithNamedAndINParameters(queryRaw string, parameters interface{}) (string, []interface{}, error) {
	query, args, err := sqlx.Named(queryRaw, parameters)
	if err != nil {
		return "", nil, err
	}
	query, args, err = sqlx.In(query, args...)
	if err != nil {
		return "", nil, err
	}
	query = r.Rebind(query)
	return query, args, err
}

func (r *Repository) CreatePhoto(photoExt string) (*feedback.Photo, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return nil, fmt.Errorf("error while create tour uuid: %w", err)
	}
	var p feedback.Photo
	if err := r.Get(&p, r.queries["create-feedback-photo"], id, id.String()+photoExt); err != nil {
		return nil, fmt.Errorf("error while create photo: %w", err)
	}
	return &p, nil
}

func (r *Repository) DeletePhoto(id string) error {
	if _, err := r.Exec(r.queries["delete-feedback-photo"], id); err != nil { //TODO: fix
		return fmt.Errorf("error while delete photo: %w", err)
	}
	return nil
}

func (r *Repository) GetGuideEstimations(guideId uuid.UUID) (*feedback.GuideEstimations, error) {
	var counts feedback.GuideEstimations
	if err := r.Get(&counts, r.queries["get-estimations-count-by-guide"], guideId); err != nil {
		if err == sql.ErrNoRows {
			return nil, common.NotFoundError{Object: "Feedbacks"}
		}
		return nil, fmt.Errorf("error while get guide estimations: %w", err)
	}
	return &counts, nil
}

func (r *Repository) GetTourEstimations(tourId uuid.UUID) (*feedback.TourEstimations, error) {
	var counts feedback.TourEstimations
	if err := r.Get(&counts, r.queries["get-estimations-count-by-tour"], tourId); err != nil {
		if err == sql.ErrNoRows {
			return nil, common.NotFoundError{Object: "Feedbacks"}
		}
		return nil, fmt.Errorf("error while get guide estimations: %w", err)
	}
	return &counts, nil
}

func (r *Repository) CheckFeedbackExistsByBookingId(bookingId uuid.UUID) (bool, error) {
	var exists bool
	if err := r.Get(&exists, r.queries["check-feedback-exists-by-booking-id"], bookingId); err != nil {
		return false, fmt.Errorf("error while check feedback exists: %w", err)
	}
	return exists, nil
}

func (r *Repository) CheckBookingExists(bookingId uuid.UUID) (bool, error) {
	var exists bool
	if err := r.Get(&exists, r.queries["check-booking-exists"], bookingId); err != nil {
		return false, fmt.Errorf("error while check feedback exists: %w", err)
	}
	return exists, nil
}

type rawFeedback struct {
	Id         uuid.UUID      `db:"id"`
	Name       string         `db:"name"`
	Comment    string         `db:"comment"`
	TourRating int            `db:"tour_rating"`
	CreatedAt  time.Time      `db:"created_at"`
	RawPhotos  sql.NullString `db:"photos"`

	NotImpressed  bool `db:"not_impressed_estimation"`
	BadWeather    bool `db:"bad_weather_estimation"`
	Uncomfortable bool `db:"uncomfortable_estimation"`
	Picturesque   bool `db:"picturesque_estimation"`
	Comfortable   bool `db:"comfortable_estimation"`
	Adrenaline    bool `db:"adrenaline_estimation"`
}

func (r *Repository) GetTourFeedbacks(tourId uuid.UUID) ([]feedback.FeedbackInList, error) {

	var rawFeedbacks []rawFeedback
	if err := r.Select(&rawFeedbacks, r.queries["get-feedbacks-by-tour"], tourId); err != nil {
		return nil, fmt.Errorf("error while get feedback list: %w", err)
	}
	var feedbacks = make([]feedback.FeedbackInList, 0)
	for _, rawFeedback := range rawFeedbacks {
		var photos []string
		if rawFeedback.RawPhotos.Valid {
			photos = strings.Split(rawFeedback.RawPhotos.String, "|")
		} else {
			photos = make([]string, 0)
		}
		feedbacks = append(feedbacks, feedback.FeedbackInList{
			Id:            rawFeedback.Id,
			Name:          rawFeedback.Name,
			Comment:       rawFeedback.Comment,
			TourRating:    rawFeedback.TourRating,
			CreatedAt:     rawFeedback.CreatedAt,
			Photos:        photos,
			NotImpressed:  rawFeedback.NotImpressed,
			BadWeather:    rawFeedback.BadWeather,
			Uncomfortable: rawFeedback.Uncomfortable,
			Picturesque:   rawFeedback.Picturesque,
			Comfortable:   rawFeedback.Comfortable,
			Adrenaline:    rawFeedback.Adrenaline,
		})
	}
	return feedbacks, nil
}

func (r *Repository) GetAllFeedbacks() ([]feedback.FeedbackInList, error) {
	var rawFeedbacks []rawFeedback
	if err := r.Select(&rawFeedbacks, r.queries["get-all-feedbacks"]); err != nil {
		return nil, fmt.Errorf("error while get feedback list: %w", err)
	}
	var feedbacks = make([]feedback.FeedbackInList, 0)
	for _, rawFeedback := range rawFeedbacks {
		var photos []string
		if rawFeedback.RawPhotos.Valid {
			photos = strings.Split(rawFeedback.RawPhotos.String, "|")
		} else {
			photos = make([]string, 0)
		}
		feedbacks = append(feedbacks, feedback.FeedbackInList{
			Id:            rawFeedback.Id,
			Name:          rawFeedback.Name,
			Comment:       rawFeedback.Comment,
			TourRating:    rawFeedback.TourRating,
			CreatedAt:     rawFeedback.CreatedAt,
			Photos:        photos,
			NotImpressed:  rawFeedback.NotImpressed,
			BadWeather:    rawFeedback.BadWeather,
			Uncomfortable: rawFeedback.Uncomfortable,
			Picturesque:   rawFeedback.Picturesque,
			Comfortable:   rawFeedback.Comfortable,
			Adrenaline:    rawFeedback.Adrenaline,
		})
	}
	return feedbacks, nil
}
