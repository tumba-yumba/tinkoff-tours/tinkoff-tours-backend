-- name: create-feedback
INSERT INTO feedback
(tour_id, booking_id, comment, guide_rating, tour_rating, boring_estimation, know_nothing_estimation,
 not_heard_estimation, interesting_estimation, know_everything_estimation, entertainment_estimation, not_impressed_estimation, bad_weather_estimation, uncomfortable_estimation, picturesque_estimation, comfortable_estimation, adrenaline_estimation)
VALUES (:tour_id, :booking_id, :comment, :guide_rating, :tour_rating, :boring, :know_nothing, :not_heard, :interesting, :know_everything, :entertainment, :not_impressed, :bad_weather, :uncomfortable, :picturesque,
        :comfortable, :adrenaline)
RETURNING id;

-- name: create-feedback-photo
INSERT INTO feedback_photos
    (id, filename)
VALUES ($1, $2) RETURNING *;

-- name: delete-feedback-photo
DELETE
FROM feedback_photos
WHERE id = $1;

-- name: update-feedback-photo
UPDATE feedback_photos
SET feedback_id = :feedback_id
WHERE id IN (:photo_ids)
  AND feedback_id IS NULL;

-- name: get-estimations-count-by-guide
SELECT AVG(guide_rating)::numeric(2, 1) as rating, SUM(boring_estimation::int) as boring_count, SUM(know_nothing_estimation::int) as know_nothing_count, SUM(not_heard_estimation::int) as not_heard_count, SUM(interesting_estimation::int) as interesting_count, SUM(know_everything_estimation::int) as know_everything_count, SUM(entertainment_estimation::int) as entertainment_count
FROM feedback
JOIN tours t on feedback.tour_id = t.id
WHERE t.guide_id = $1
GROUP BY guide_id

-- name: get-estimations-count-by-tour
SELECT AVG(tour_rating)::numeric(2, 1) as rating, SUM(not_impressed_estimation::int) as not_impressed_count, SUM(bad_weather_estimation::int) as bad_weather_count, SUM(uncomfortable_estimation::int) as uncomfortable_count, SUM(picturesque_estimation::int) as picturesque_count, SUM(comfortable_estimation::int) as comfortable_count, SUM(adrenaline_estimation::int) as adrenaline_count
FROM feedback
WHERE tour_id = $1
GROUP BY tour_id

-- name: check-feedback-exists-by-booking-id
SELECT EXISTS (
  SELECT id
  FROM feedback
  WHERE booking_id = $1
)

-- name: check-booking-exists
SELECT EXISTS (
  SELECT id
  FROM tour_booking
  WHERE id = $1
)

-- name: get-feedbacks-by-tour
SELECT
  f.id,
  tb.name,
  f.comment,
  f.tour_rating,
  f.created_at,
  string_agg(fp.filename, '|') AS photos,
  f.not_impressed_estimation,
  f.bad_weather_estimation,
  f.uncomfortable_estimation,
  f.picturesque_estimation,
  f.comfortable_estimation,
  f.adrenaline_estimation
FROM feedback f
  LEFT JOIN feedback_photos fp ON fp.feedback_id = f.id
  JOIN tour_booking tb ON f.booking_id = tb.id
WHERE f.tour_id = $1
GROUP BY (f.id, tb.name);

-- name: get-all-feedbacks
SELECT
  f.id,
  tb.name,
  f.comment,
  f.tour_rating,
  f.created_at,
  string_agg(fp.filename, '|') AS photos,
  f.not_impressed_estimation,
  f.bad_weather_estimation,
  f.uncomfortable_estimation,
  f.picturesque_estimation,
  f.comfortable_estimation,
  f.adrenaline_estimation
FROM feedback f
  LEFT JOIN feedback_photos fp ON fp.feedback_id = f.id
  JOIN tour_booking tb ON f.booking_id = tb.id
GROUP BY (f.id, tb.name);