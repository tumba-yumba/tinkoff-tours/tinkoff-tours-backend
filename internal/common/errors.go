package common

import (
	"encoding/json"
	"fmt"
)

type DecodeError struct {
}

func (de DecodeError) Error() string {
	return "Can't decode request data"
}

type ValidationError struct {
	Field string
	Tag   string
	Value string
}

func (ve ValidationError) Error() string {
	return fmt.Sprintf("Validation error at '%s' field", ve.Field)
}

type ValidationErrors struct {
	Errors []ValidationError
}

func (ves ValidationErrors) Error() string {
	value, _ := json.Marshal(ves.Errors)
	return string(value)
}

type AlreadyExistsError struct {
	Object string
}

func (aee AlreadyExistsError) Error() string {
	return fmt.Sprintf("Object %s already exists", aee.Object)
}

type NotFoundError struct {
	Object string
}

func (nee NotFoundError) Error() string {
	return fmt.Sprintf("Object %s not exists", nee.Object)
}
