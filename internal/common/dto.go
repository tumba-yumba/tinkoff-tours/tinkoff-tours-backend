package common

type Pagination struct {
	Limit  *int `schema:"limit" db:"limit" validate:"required,gte=0" json:"limit"`
	Offset *int `schema:"offset" db:"offset" validate:"gte=0,required" json:"offset"`
}
